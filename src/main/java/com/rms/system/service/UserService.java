package com.rms.system.service;


import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.rms.system.awsS3.S3Buckets;
import com.rms.system.awsS3.S3Service;
import com.rms.system.base.BaseListingRQ;
import com.rms.system.base.BaseService;
import com.rms.system.base.StructureRS;
import com.rms.system.constant.MessageConstant;
import com.rms.system.db.entity.UserEntity;
import com.rms.system.db.entity.enumentity.UserGenderEnum;
import com.rms.system.db.repository.OrderRepository;
import com.rms.system.db.repository.RoleRepository;
import com.rms.system.db.repository.UserRepository;
import com.rms.system.exception.httpstatus.BadRequestException;
import com.rms.system.model.projection.order.OrderEntityInfo;
import com.rms.system.model.projection.user.UserEntityInfo;
import com.rms.system.model.projection.user.UserEntityInfoById;
import com.rms.system.model.projection.user.UserEntityInfoProfile;
import com.rms.system.model.request.auth.RegisterRQ;
import com.rms.system.model.request.user.*;
import com.rms.system.security.UserPrincipal;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService extends BaseService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final OrderRepository orderRepository;
    private final S3Service s3Service;
    private final S3Buckets s3Buckets;
    private final AmazonS3 s3client;

    @Transactional
    public StructureRS createUser(UserRQ userRQ) {

        if (userRepository.existsAllByUsername(userRQ.getUsername()))
            throw new BadRequestException(MessageConstant.USER.USERNAME_IS_EXIST);

        if (userRepository.existsAllByEmail(userRQ.getEmail()))
            throw new BadRequestException(MessageConstant.USER.EMAIL_IS_EXIST);

        if (!roleRepository.existsById(userRQ.getRoleId()))
            throw new BadRequestException(MessageConstant.ROLE.ROLE_NOT_FOUND);

        UserEntity user = new UserEntity();
        user.setName(userRQ.getName());
        user.setUsername(userRQ.getUsername());
        user.setGender(UserGenderEnum.valueOf(userRQ.getGender()));
        user.setEmail(userRQ.getEmail());
        user.setPassword(this.passwordEncoder.encode(userRQ.getPassword()));
        user.setPhone(userRQ.getPhone());
        user.setSalary(userRQ.getSalary());
        user.setHireDate(userRQ.getHireDate());
        user.setAvatar(userRQ.getAvatar());
        user.setRoleEntity(roleRepository.getReferenceById(userRQ.getRoleId()));
        userRepository.save(user);

        return response(MessageConstant.USER.USER_IS_CREATED);
    }

    public StructureRS getUsers(BaseListingRQ request, Long roleId) {
        if (roleId != 0 && !roleRepository.existsById(roleId))
            throw new BadRequestException(MessageConstant.ROLE.ROLE_NOT_FOUND);

        Page<UserEntityInfo> users = userRepository.findUsers(request.getQuery(), roleId, request.getPageable(request.getSort(), request.getOrder()));
        return response(users.getContent(), users);
    }

    public StructureRS deleteUser(Long id) {
        Optional<UserEntity> user = userRepository.findByIdEqualsAndDeletedAtNull(id);

        if (id == 1)
            throw new BadRequestException(MessageConstant.USER.ADMIN_USER);

        if (user.isPresent()) {
            userRepository.deleteById(user.get().getId());
            return response(MessageConstant.USER.USER_IS_DELETED);
        }
        throw new BadRequestException(MessageConstant.USER.NOT_FOUND);
    }

    public StructureRS updateStatus(Long id) {
        Optional<UserEntity> user = userRepository.findByIdEqualsAndDeletedAtNull(id);

        if (id == 1)
            throw new BadRequestException(MessageConstant.USER.ADMIN_USER);

        user.ifPresent(userEntity -> {
            userEntity.setStatus(!userEntity.getStatus());
            userRepository.save(userEntity);
        });

        return response(MessageConstant.USER.USER_IS_UPDATE);
    }

    public StructureRS updateUser(Long id, UpdateUserRQ request) {
        Optional<UserEntity> userEntity = userRepository.findByIdEqualsAndDeletedAtNull(id);

        if (userEntity.isEmpty())
            throw new BadRequestException(MessageConstant.USER.NOT_FOUND);

        if (!roleRepository.existsById(request.getRoleId()))
            throw new BadRequestException(MessageConstant.ROLE.ROLE_NOT_FOUND);

        UserEntity user = userEntity.get();
        user.setName(request.getName());
        user.setGender(UserGenderEnum.valueOf(request.getGender()));
        user.setPhone(request.getPhone());
        user.setAvatar(request.getAvatar());
        user.setRoleEntity(this.roleRepository.getReferenceById(request.getRoleId()));
        userRepository.save(user);

        return response(MessageConstant.USER.USER_IS_UPDATE);
    }

    public StructureRS changePasswordUser(Long id, PasswordChangeRQ request) {
        Optional<UserEntity> userEntity = userRepository.findByIdEqualsAndDeletedAtNull(id);
        if (userEntity.isEmpty())
            throw new BadRequestException(MessageConstant.USER.NOT_FOUND);

        if (id == 1)
            throw new BadRequestException(MessageConstant.USER.ADMIN_USER);

        UserEntity user = userEntity.get();
        user.setPassword(this.passwordEncoder.encode(request.getPassword()));
        userRepository.save(user);
        return response(MessageConstant.USER.USER_IS_UPDATE);
    }

    public StructureRS resetPassword(PasswordResetRQ request, UserPrincipal principal) {
        Optional<UserEntity> userEntity = userRepository.findById(principal.getId());

        if (userEntity.isEmpty())
            throw new BadRequestException(MessageConstant.USER.NOT_FOUND);

        if (principal.getId() == 1)
            throw new BadRequestException(MessageConstant.USER.ADMIN_USER);

        if (!passwordEncoder.matches(request.getOldPassword(), userEntity.get().getPassword()))
            throw new BadRequestException(MessageConstant.USER.OLD_PASSWORD_WRONG);

        if (passwordEncoder.matches(request.getPassword(), userEntity.get().getPassword()))
            throw new BadRequestException(MessageConstant.USER.SAME_PASSWORD);

        UserEntity user = userEntity.get();
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        userRepository.save(user);

        return response(MessageConstant.USER.USER_IS_UPDATE);
    }

    public StructureRS profile(UserPrincipal principal) {
        Optional<UserEntityInfoProfile> userEntity = userRepository.findByIdTokenEqualsAndStatusTrue(principal.getId());
        if (userEntity.isEmpty())
            throw new BadRequestException(MessageConstant.USER.NOT_FOUND);
        return response(userEntity.get());
    }

    public StructureRS updateProfile(UpdateProfileRQ profileRQ, UserPrincipal principal) {
        Optional<UserEntity> userEntity = userRepository.findById(principal.getId());

        if (userEntity.isEmpty())
            throw new BadRequestException(MessageConstant.USER.NOT_FOUND);

        if (principal.getId() == 1)
            throw new BadRequestException(MessageConstant.USER.ADMIN_USER);

        UserEntity user = userEntity.get();
        user.setName(profileRQ.getName());
        user.setGender(UserGenderEnum.valueOf(profileRQ.getGender()));
        user.setPhone(profileRQ.getPhone());
        userRepository.save(user);

        return response(MessageConstant.USER.PROFILE_UPDATED);
    }

    public void uploadFileTos3bucket(String fileName, File file) {
        s3client.putObject(new PutObjectRequest(s3Buckets.getName(), fileName, file)
                .withCannedAcl(CannedAccessControlList.PublicRead));
    }
    @Transactional
    public StructureRS uploadProfileAvatar(Long id, MultipartFile multipartFile) throws IOException {
        Optional<UserEntity> userEntity = userRepository.findById(id);

        if (userEntity.isEmpty())
            throw new BadRequestException(MessageConstant.USER.NOT_FOUND);

        if (multipartFile.isEmpty())
            throw new BadRequestException(MessageConstant.USER.PROFILE_IMAGE_NOT_FOUND);

        if (
                !multipartFile.getContentType().equals("image/jpeg") &&
                !multipartFile.getContentType().equals("image/jpg") &&
                !multipartFile.getContentType().equals("image/png")
        )
            throw new BadRequestException(MessageConstant.USER.PROFILE_IMAGE_NOT_VALID);
        if (multipartFile.getSize() > 1024 * 1024 * 10)
            throw new BadRequestException(MessageConstant.USER.PROFILE_IMAGE_TOO_LARGE);

        String avatar = "";
        String getAvatar = userEntity.get().getAvatar();

        if (getAvatar != null) {
            avatar = "user-avatar/" + getAvatar.substring(getAvatar.lastIndexOf("/") + 1);
            DeleteObjectRequest request = new DeleteObjectRequest(s3Buckets.getName(), avatar);
            this.s3client.deleteObject(request);
        }
        System.out.println(avatar);

        String fileUrl = "";

        try {
            File file = s3Service.convertMultiPartToFile(multipartFile);
            String fileName = "user-avatar/" + id + s3Service.generateFileName(multipartFile);
            fileUrl = s3Buckets.getEndPointUrl() + "/" + s3Buckets.getName() + "/" + fileName;
            uploadFileTos3bucket(fileName, file);
            file.delete();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }

        userEntity.get().setAvatar(fileUrl);
        userRepository.save(userEntity.get());

        return response(userEntity.get().getAvatar());
    }

    public StructureRS getUserById(Long id) {
        Optional<UserEntityInfoById> optionalUserEntityInfo = userRepository.findByIdEqualsAndStatusTrue(id);

        if (optionalUserEntityInfo.isEmpty())
            throw new BadRequestException(MessageConstant.USER.NOT_FOUND);

        return response(optionalUserEntityInfo.get());
    }

    @Transactional
    public StructureRS uploadProfileImageWithUserToken(UserPrincipal principal, MultipartFile multipartFile) {
        Optional<UserEntity> userEntity = userRepository.findById(principal.getId());

        if (userEntity.isEmpty())
            throw new BadRequestException(MessageConstant.USER.NOT_FOUND);

        if (multipartFile.isEmpty())
            throw new BadRequestException(MessageConstant.USER.PROFILE_IMAGE_NOT_FOUND);

        if (
                !multipartFile.getContentType().equals("image/jpeg") &&
                        !multipartFile.getContentType().equals("image/jpg") &&
                        !multipartFile.getContentType().equals("image/png")
        )
            throw new BadRequestException(MessageConstant.USER.PROFILE_IMAGE_NOT_VALID);
        if (multipartFile.getSize() > 1024 * 1024 * 10)
            throw new BadRequestException(MessageConstant.USER.PROFILE_IMAGE_TOO_LARGE);

        String avatar = "";
        String getAvatar = userEntity.get().getAvatar();

        if (getAvatar != null) {
            avatar = "user-avatar/" + getAvatar.substring(getAvatar.lastIndexOf("/") + 1);
            DeleteObjectRequest request = new DeleteObjectRequest(s3Buckets.getName(), avatar);
            this.s3client.deleteObject(request);
        }
        System.out.println(avatar);

        String fileUrl = "";

        try {
            File file = s3Service.convertMultiPartToFile(multipartFile);
            String fileName = "user-avatar/" + principal.getId() + s3Service.generateFileName(multipartFile);
            fileUrl = s3Buckets.getEndPointUrl() + "/" + s3Buckets.getName() + "/" + fileName;
            uploadFileTos3bucket(fileName, file);
            file.delete();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }

        userEntity.get().setAvatar(fileUrl);
        userRepository.save(userEntity.get());

        return response(userEntity.get().getAvatar());
    }

    public StructureRS deleteProfileAvatar(UserPrincipal principal) {
        Optional<UserEntity> optionalUserEntity = userRepository.findById(principal.getId());

        if (optionalUserEntity.isEmpty())
            throw new BadRequestException(MessageConstant.USER.NOT_FOUND);

        optionalUserEntity.get().setAvatar(null);
        userRepository.save(optionalUserEntity.get());
        return response();
    }

    public StructureRS getUserOrder(BaseListingRQ request, UserPrincipal principal) {
        Optional<UserEntity> optionalUserEntity = userRepository.findById(principal.getId());
        Page<OrderEntityInfo> optionalOrderRepository = orderRepository.findByIdEqualsAndDeletedAtNull(principal.getId(), request.getPageable());

        if (optionalUserEntity.isEmpty())
            throw new BadRequestException(MessageConstant.USER.NOT_FOUND);


        return response(optionalOrderRepository.getContent(), optionalOrderRepository);
    }

    public StructureRS getUserCustomer(BaseListingRQ request) {

        Page<UserEntityInfo> getUserCustomer = userRepository.findCustomers(request.getQuery(), request.getPageable(request.getSort(), request.getOrder()));

        return response(getUserCustomer.getContent(), getUserCustomer);
    }

    public void register(RegisterRQ registerRQ) {

        if (userRepository.existsAllByUsername(registerRQ.getUsername()))
            throw new BadRequestException(MessageConstant.USER.USERNAME_IS_EXIST);

        if (userRepository.existsAllByEmail(registerRQ.getEmail()))
            throw new BadRequestException(MessageConstant.USER.EMAIL_IS_EXIST);

        UserEntity user = new UserEntity();
        user.setName(registerRQ.getName());
        user.setUsername(registerRQ.getUsername());
        user.setGender(UserGenderEnum.valueOf(registerRQ.getGender()));
        user.setEmail(registerRQ.getEmail());
        user.setPassword(this.passwordEncoder.encode(registerRQ.getPassword()));
        user.setPhone(registerRQ.getPhone());
        user.setRoleEntity(roleRepository.getReferenceById(2L));
        user.setHireDate(Instant.now());
        userRepository.save(user);
    }

    public StructureRS updateUserCustomer(UpdateUserCustomer request, UserPrincipal principal) {
        Optional<UserEntity> userEntity = userRepository.findById(principal.getId());

        if (userEntity.isEmpty())
            throw new BadRequestException(MessageConstant.USER.NOT_FOUND);

        if (principal.getId() == 1)
            throw new BadRequestException(MessageConstant.USER.ADMIN_USER);

        UserEntity user = userEntity.get();
        user.setName(request.getName());
        user.setUsername(request.getUsername());
        user.setEmail(request.getEmail());
        user.setGender(UserGenderEnum.valueOf(request.getGender()));
        user.setPhone(request.getPhone());
        user.setAvatar(request.getAvatar());
        userRepository.save(user);

        return response(MessageConstant.USER.PROFILE_UPDATED);
    }
}
