package com.rms.system.service;

import com.rms.system.base.BaseListingRQ;
import com.rms.system.base.BaseService;
import com.rms.system.base.StructureRS;
import com.rms.system.constant.MessageConstant;
import com.rms.system.db.entity.AddressEntity;
import com.rms.system.db.entity.UserEntity;
import com.rms.system.db.repository.AddressRepository;
import com.rms.system.db.repository.UserRepository;
import com.rms.system.exception.httpstatus.BadRequestException;
import com.rms.system.model.projection.address.AddressEntityInfo;
import com.rms.system.model.request.address.AddressRQ;
import com.rms.system.security.UserPrincipal;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AddressService extends BaseService {

    private final AddressRepository addressRepository;
    private final UserRepository userRepository;

    public StructureRS createAddressByUser(Long userId, AddressRQ addressRQ) {
        Optional<UserEntity> optionalUserEntity = userRepository.findByIdEqualsAndDeletedAtNull(userId);

        if (optionalUserEntity.isEmpty())
            throw new BadRequestException(MessageConstant.USER.NOT_FOUND);

        if (addressRepository.existsByNameEqualsAndIdEquals(addressRQ.getName(), userId))
            throw new BadRequestException(MessageConstant.ADDRESS.ADDRESS_IS_EXISTED);

        AddressEntity addressEntity = new AddressEntity();
        addressEntity.setName(addressRQ.getName());
        addressEntity.setLocation(addressRQ.getLocation());
        addressEntity.setGoogleMapUrl(addressRQ.getGoogleMapUrl());
        addressEntity.setUserEntity(userRepository.getReferenceById(optionalUserEntity.get().getId()));

        addressRepository.save(addressEntity);

        return response(MessageConstant.ADDRESS.ADDRESS_IS_CREATED);
    }

    public StructureRS getAddressByUserToken(BaseListingRQ request, UserPrincipal principal) {
        Optional<UserEntity> optionalUserEntity = userRepository.findById(principal.getId());

        if (optionalUserEntity.isEmpty())
            throw new BadRequestException(MessageConstant.USER.NOT_FOUND);

        Page<AddressEntityInfo> addressEntity = addressRepository.findByUserEntity_IdEquals(optionalUserEntity.get().getId(), request.getPageable(request.getSort(), request.getOrder()));

        return response(addressEntity.getContent(), addressEntity);
    }

    public StructureRS createAddressByUserLogin(AddressRQ addressRQ, UserPrincipal principal) {
        Optional<UserEntity> optionalUserEntity = userRepository.findById(principal.getId());
        if (optionalUserEntity.isEmpty())
            throw new BadRequestException(MessageConstant.USER.NOT_FOUND);
        AddressEntity addressEntity = new AddressEntity();
        addressEntity.setName(addressRQ.getName());
        addressEntity.setLocation(addressRQ.getLocation());
        addressEntity.setGoogleMapUrl(addressRQ.getGoogleMapUrl());
        addressEntity.setUserEntity(userRepository.getReferenceById(optionalUserEntity.get().getId()));
        addressRepository.save(addressEntity);

        return response(MessageConstant.ADDRESS.ADDRESS_IS_CREATED);
    }

    public StructureRS deleteAddressByUserToken(Long id, UserPrincipal build) {
        Optional<UserEntity> user = userRepository.findById(build.getId());
        Optional<AddressEntity> address = addressRepository.findByIdAndDeletedAtIsNull(id);

        if (address.isEmpty())
            throw new BadRequestException(MessageConstant.ADDRESS.ADDRESS_NOT_FOUND);

        if (user.isPresent()) {
            addressRepository.deleteById(address.get().getId());
            return response(MessageConstant.ADDRESS.ADDRESS_IS_DELETED);
        }

        throw new BadRequestException(MessageConstant.USER.NOT_FOUND);
    }

    public StructureRS getAddressById(UserPrincipal principal) {

        Optional<UserEntity> optionalUserEntity = userRepository.findById(principal.getId());
        Optional<AddressEntityInfo> addressEntityInfo = addressRepository.findAddressEntityInfoByUserId(principal.getId());

        if (optionalUserEntity.isEmpty())
            throw new BadRequestException(MessageConstant.USER.NOT_FOUND);

        if (addressEntityInfo.isEmpty())
            throw new BadRequestException(MessageConstant.ADDRESS.ADDRESS_NOT_FOUND);

        return response(addressEntityInfo.get());
    }
}
