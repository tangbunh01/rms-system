package com.rms.system.service;

import com.rms.system.base.BaseListingRQ;
import com.rms.system.base.BaseService;
import com.rms.system.base.StructureRS;
import com.rms.system.constant.MessageConstant;
import com.rms.system.db.entity.FoodEntity;
import com.rms.system.db.entity.FoodImageEntity;
import com.rms.system.db.repository.FoodCategoryEntityRepository;
import com.rms.system.db.repository.FoodEntityRepository;
import com.rms.system.db.repository.FoodImageEntityRepository;
import com.rms.system.exception.httpstatus.BadRequestException;
import com.rms.system.model.projection.food.FoodEntityInfo;
import com.rms.system.model.request.food.FoodRQ;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class FoodService extends BaseService {
    private final FoodEntityRepository foodEntityRepository;
    private final FoodCategoryEntityRepository foodCategoryEntityRepository;
    private final FoodImageEntityRepository foodImageEntityRepository;

    public StructureRS getFoods(BaseListingRQ request) {
        Page<FoodEntityInfo> foodEntityInfos = foodEntityRepository.findByNameLike(
                request.getQuery(), request.getPageable(request.getSort(),request.getOrder()));
        return response(foodEntityInfos.getContent(),foodEntityInfos);
    }

    public StructureRS getByFoodId(Long id) {
        if (!foodEntityRepository.existByIdAndDeleteIsNull(id))
            throw new BadRequestException(MessageConstant.FOOD.FOOD_NOT_FOUND);
        FoodEntityInfo foodEntityInfo = foodEntityRepository.findOneById(id);
        return response(foodEntityInfo);
    }

    public StructureRS addFood(FoodRQ foodRQ) {
        if (foodEntityRepository.existsByCodeEquals(foodRQ.getCode()))
            throw new BadRequestException(MessageConstant.FOOD.FOOD_CODE_EXISTS);

        FoodEntity foodEntity = new FoodEntity();
        if (!foodCategoryEntityRepository.existByIds(foodRQ.getFoodCategoryId()))
            throw new  BadRequestException(MessageConstant.FOOD.FOOD_CONSTRAINT);
        ForeignKeyFood(foodRQ, foodEntity);
        Optional<Long> foodId = foodEntityRepository.findFoodByIdEquals(foodEntity.getId());

        Map<String,Long> showId = new HashMap<>();
        foodId.ifPresent(id -> showId.put("id",id));

        return  response(showId);
    }

    public void updateFood(FoodRQ foodRQ, Long id) {
        Optional<FoodEntity> foodEntities = foodEntityRepository.findByIdAndDeletedAtIsNull(id);
        List<FoodImageEntity> foodImageEntities = foodImageEntityRepository.findByFoodImageEntityIdEquals(id);

        if (foodEntities.isEmpty())
            throw new BadRequestException(MessageConstant.FOOD.FOOD_NOT_FOUND);

        if(!foodCategoryEntityRepository.existByIds(foodRQ.getFoodCategoryId())){
            throw new BadRequestException(MessageConstant.FOOD.FOOD_CONSTRAINT);
        }
        if (foodEntityRepository.existsByCodeEqualsAndIdNotEquals(foodRQ.getCode(), id))
            throw new BadRequestException(MessageConstant.FOOD.FOOD_CODE_EXISTS);

        FoodEntity foodEntity = foodEntities.get();
        for (FoodImageEntity foodImageEntity : foodImageEntities)
            foodImageEntityRepository.delete(foodImageEntity);
        foodEntity.setName(foodRQ.getName());
        foodEntity.setCode(foodRQ.getCode());
        foodEntity.setPrice(foodRQ.getPrice());
        foodEntity.setDiscount(foodRQ.getDiscount());
        foodEntity.setStatus(foodRQ.getStatus());
        foodEntity.setDescription(foodRQ.getDescription());
        foodEntity.setFoodCategoryEntity(foodCategoryEntityRepository.getReferenceById(foodRQ.getFoodCategoryId()));

        foodEntityRepository.save(foodEntity);
    }

    public void deleteFood(Long id) {
        Optional<FoodEntity> foodEntityInfo = foodEntityRepository.findByIdAndDeletedAtIsNull(id);
        if (foodEntityInfo.isEmpty())
            throw new BadRequestException(MessageConstant.FOOD.FOOD_NOT_FOUND);
        foodEntityRepository.deleteById(id);
    }

    // filter function
    public StructureRS filterFoods( String filters, String name) {
        List<FoodEntityInfo> foodEntityFilters;
        Instant currentDate = LocalDate.now().minusYears(1).atStartOfDay(ZoneId.systemDefault()).toInstant();

        if (filters == null)
            throw new BadRequestException(MessageConstant.FOOD.INVALID_FILTER);
        switch(filters) {
            case "popular":
                foodEntityFilters = foodEntityRepository.findPopularFood();
                break;
            case "new_release":
                foodEntityFilters = foodEntityRepository.findByCreatedDate(currentDate);
                break;
            case "categories":
                if (name != null)
                    foodEntityFilters = foodEntityRepository.findByFoodCategoryEntity_Name(name);
                else
                    foodEntityFilters = foodEntityRepository.findByFoodCategoryEntity();
                break;

            default:
                throw new BadRequestException (MessageConstant.FOOD.INVALID_FILTER);
        }
        return response(foodEntityFilters);
    }

    private void ForeignKeyFood(FoodRQ foodRQ, FoodEntity foodEntity) {
        foodEntity.setName(foodRQ.getName());
        foodEntity.setCode(foodRQ.getCode());
        foodEntity.setPrice(foodRQ.getPrice());
        foodEntity.setDiscount(foodRQ.getDiscount());
        foodEntity.setStatus(Boolean.TRUE);
        foodEntity.setDescription(foodRQ.getDescription());
        foodEntity.setFoodCategoryEntity(foodCategoryEntityRepository.getReferenceById(foodRQ.getFoodCategoryId()));
        foodEntityRepository.save(foodEntity);
    }
}
