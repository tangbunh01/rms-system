package com.rms.system.service;


import com.rms.system.base.BaseListingRQ;
import com.rms.system.base.BaseService;
import com.rms.system.base.StructureRS;
import com.rms.system.constant.MessageConstant;
import com.rms.system.db.entity.TableEntity;
import com.rms.system.db.entity.enumentity.TableStatusEnum;
import com.rms.system.db.repository.TableRepository;
import com.rms.system.exception.httpstatus.BadRequestException;
import com.rms.system.model.projection.table.TableEntityInfo;
import com.rms.system.model.request.table.TableRQ;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TableService extends BaseService {

    private final TableRepository tableRepository;

    public StructureRS getTable(BaseListingRQ request, TableStatusEnum status){
        Page<TableEntityInfo> tableEntityInfo = tableRepository.findByNameStartsWithOrSeatCapacityLikeAndStatusEquals(
                request.getQuery(),
                status,
                request.getPageable(request.getSort(),request.getOrder()));
        return response(tableEntityInfo.getContent(),tableEntityInfo);
    }
    public StructureRS getTableById(Long id) {
        if (!tableRepository.existsByIdAndDeletedAtIsNull(id))
            throw new BadRequestException(MessageConstant.FOOD.FOOD_NOT_FOUND);
        TableEntityInfo tableEntityInfo = tableRepository.findOneById(id);
        return response(tableEntityInfo);
    }

    public StructureRS createTable(TableRQ tableRQ) {
        if(tableRepository.existsByName(tableRQ.getName()))
            throw new BadRequestException(MessageConstant.TABLE.TABLE_EXISTS);
        if (tableRQ.getStatus() == TableStatusEnum.Available || tableRQ.getStatus() == TableStatusEnum.Booked) {
            TableEntity tableEntity = new TableEntity();
            tableEntity.setName(tableRQ.getName());
            tableEntity.setSeatCapacity(tableRQ.getSeatCapacity());
            tableEntity.setStatus(tableRQ.getStatus());
            tableRepository.save(tableEntity);
        } else {
            throw new BadRequestException(MessageConstant.TABLE.CANNOT_CREATE);

        }
        return response(MessageConstant.TABLE.TABLE_CREATED);
    }

    public StructureRS updateTable(Long id, TableRQ tableRQ) {
        Optional<TableEntity> tableEntity = tableRepository.findById(id);
        if (tableEntity.isPresent()){
            TableEntity table= tableEntity.get();
            table.setName(tableRQ.getName());
            table.setSeatCapacity(tableRQ.getSeatCapacity());
            table.setStatus(tableRQ.getStatus());
            tableRepository.save(table);
            return response(MessageConstant.SUCCESSFULLY);
        }
        throw new BadRequestException(MessageConstant.TABLE.TABLE_NOT_FOUND);
    }

    public StructureRS deleteTable(Long id) {
        Boolean isTableValid = tableRepository.checkTableExistsAndNotDeleted(id);
        if (isTableValid == null)
            throw new BadRequestException(MessageConstant.TABLE.TABLE_NOT_FOUND);
        else if (!isTableValid)
            throw new BadRequestException(MessageConstant.TABLE.TABLE_NOT_FOUND);
        else
            tableRepository.deleteById(id);
        return response(MessageConstant.SUCCESSFULLY);
    }
}
