package com.rms.system.service;

import com.rms.system.base.BaseListingRQ;
import com.rms.system.base.BaseService;
import com.rms.system.base.StructureRS;
import com.rms.system.constant.MessageConstant;
import com.rms.system.db.entity.FoodEntity;
import com.rms.system.db.entity.OrderEntity;
import com.rms.system.db.entity.OrderItemEntity;
import com.rms.system.db.entity.enumentity.OrderPaymentMethodEnum;
import com.rms.system.db.entity.enumentity.OrderStatusEnum;
import com.rms.system.db.repository.*;
import com.rms.system.exception.httpstatus.BadRequestException;
import com.rms.system.model.projection.order.OrderEntityInfo;
import com.rms.system.model.projection.order.OrderEntityInfoById;
import com.rms.system.model.request.order.OrderItemRQ;
import com.rms.system.model.request.order.OrderRQ;
import com.rms.system.model.request.order.OrderStatusRQ;
import com.rms.system.model.request.order.PaymentStatusRQ;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderService extends BaseService {

    private final OrderRepository orderRepository;
    private final UserRepository userRepository;
    private final TableRepository tableRepository;
    private final OrderItemEntityRepository orderItemEntityRepository;
    private final FoodEntityRepository foodEntityRepository;

    public StructureRS getOrder(BaseListingRQ request, OrderStatusEnum status, OrderPaymentMethodEnum paymentMethod) {
        Page<OrderEntityInfo> orderEntityInfos = orderRepository.findByTableEntityNameAndStatusAndPaymentMethod(
                request.getQuery(), status, paymentMethod, request.getPageable(request.getSort(), request.getOrder())
        );
        return response(orderEntityInfos.getContent(), orderEntityInfos);
    }

    public StructureRS getOrderById(Long id, BaseListingRQ request) {
        Optional<OrderEntity> orderEntity = orderRepository.findByIdAndDeletedAtIsNull(id);

        if (orderEntity.isEmpty())
            throw new BadRequestException(MessageConstant.ORDER.ID_NOT_FOUND);

        OrderEntity orderEntity1 = orderEntity.get();
        Page<OrderEntityInfoById> order = orderRepository.findByOrderEntityId(orderEntity1.getId(),
                request.getPageable(request.getSort(), request.getOrder()));
        return response(order.getContent());
    }

    public StructureRS createOrder(OrderRQ orderRQ) {
        if (orderRQ.getTableId() != null)
            if (!tableRepository.existsByIdAndDeletedAtIsNull(orderRQ.getTableId()))
                throw new BadRequestException(MessageConstant.O_ITEM.O_ITEM_CONSTRAINT_TABLE);
        if (!userRepository.existsById(orderRQ.getUserId()))
            throw new BadRequestException(MessageConstant.O_ITEM.O_ITEM_CONSTRAINT_USER);
        List<Long> foodIds = orderRQ.getItems().stream().map(OrderItemRQ::getFoodId).toList();
        if (!foodEntityRepository.existsAllById(foodIds))
            throw new BadRequestException(MessageConstant.O_ITEM.O_ITEM_CONSTRAINT_FOOD);
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setUserEntity(userRepository.getReferenceById(orderRQ.getUserId()));
        if (orderRQ.getTableId() != null)
            orderEntity.setTableEntity(tableRepository.getReferenceById(orderRQ.getTableId()));
        orderEntity.setPaymentMethod(OrderPaymentMethodEnum.Cash);
        orderEntity.setStatus(OrderStatusEnum.Prepare);
        orderEntity.setTotalPrice(0.00);
        orderRepository.save(orderEntity);


        List<FoodEntity> foodEntities = foodEntityRepository.findByIdIn(orderRQ.getItems().stream().map(OrderItemRQ::getFoodId).toList());

        List<OrderItemEntity> orderItemEntities = new ArrayList<>();
        double totalOrderPrice = 0.0;
        for (OrderItemRQ item : orderRQ.getItems()) {
            OrderItemEntity orderItemEntity = new OrderItemEntity();
            Long foodId = item.getFoodId();
            Optional<FoodEntity> foodEntity = foodEntities.stream().filter(it -> it.getId().equals(foodId)).findFirst();
            if (foodEntity.isPresent()) {
                FoodEntity food = foodEntity.get();
                double foodPrice = food.getPrice();
                Integer qty = item.getQuantity();
                double discount = food.getDiscount();
                double price = foodPrice * qty;
                double subTotal = price - (price * discount / 100);
                totalOrderPrice += subTotal;
                orderItemEntity.setFoodEntity(food);
                orderItemEntity.setOrderEntity(orderRepository.getReferenceById(orderEntity.getId()));
                orderItemEntity.setUnitPrice(foodPrice);
                orderItemEntity.setQuantity(qty);
                orderItemEntity.setTotalPrice(subTotal);
                orderItemEntities.add(orderItemEntity);
            } else {
                orderRepository.delete(orderEntity);
                throw new BadRequestException(MessageConstant.FOOD.FOOD_NOT_FOUND);
            }
        }
        orderItemEntityRepository.saveAll(orderItemEntities);
        orderEntity.setTotalPrice(totalOrderPrice);
        orderRepository.save(orderEntity);
        return response(MessageConstant.ORDER.ORDER_CREATED);
    }

    public StructureRS updateOrder(OrderRQ orderRQ, Long id) {
        if (!userRepository.existsById(orderRQ.getUserId()))
            throw new BadRequestException(MessageConstant.USER.NOT_FOUND);
        if (!tableRepository.existsById(orderRQ.getTableId()))
            throw new BadRequestException(MessageConstant.TABLE.ID_NOT_FOUND);
        if (!orderRepository.existsById(id))
            throw new BadRequestException(MessageConstant.ORDER.ORDER_NOT_FOUND);

        Optional<OrderEntity> orderEntityOptional = orderRepository.findById(id);
        if (orderEntityOptional.isPresent()) {
            OrderEntity orderEntity = orderEntityOptional.get();
            orderEntity.setUserEntity(userRepository.getReferenceById(orderRQ.getUserId()));
            orderEntity.setTableEntity(tableRepository.getReferenceById(orderRQ.getTableId()));
            orderEntity.setPaymentMethod(OrderPaymentMethodEnum.Cash);
            orderEntity.setStatus(OrderStatusEnum.Prepare);
            orderEntity.setTotalPrice(0.00);
            orderItemEntityRepository.deleteByOrderEntity_Id(id);
            orderRepository.save(orderEntity);

            List<OrderItemEntity> orderItemEntities = new ArrayList<>();
            double totalOrderPrice = 0.0;
            for (OrderItemRQ item : orderRQ.getItems()) {
                Long foodId = item.getFoodId();
                FoodEntity foodEntity = foodEntityRepository.findById(foodId)
                        .orElseThrow(() -> new BadRequestException(MessageConstant.O_ITEM.O_ITEM_CONSTRAINT_FOOD));

                double foodPrice = foodEntity.getPrice();
                Integer qty = item.getQuantity();
                double discount = foodEntity.getDiscount();
                double price = foodPrice * qty;
                double subTotal = price - (price * discount / 100);
                totalOrderPrice += subTotal;

                OrderItemEntity orderItemEntity = new OrderItemEntity();
                orderItemEntity.setFoodEntity(foodEntity);
                orderItemEntity.setOrderEntity(orderEntity);
                orderItemEntity.setUnitPrice(foodPrice);
                orderItemEntity.setQuantity(qty);
                orderItemEntity.setTotalPrice(subTotal);
                orderItemEntities.add(orderItemEntity);
            }
            orderItemEntityRepository.saveAll(orderItemEntities);
            orderEntity.setTotalPrice(totalOrderPrice);
            orderRepository.save(orderEntity);
        }
        return response(MessageConstant.ORDER.ORDER_UPDATED);
    }

    public StructureRS deleteOrder(Long id) {
        Boolean isOrderValid = orderRepository.checkOrderExistsAndNotDeleted(id);
        if (isOrderValid == null)
            throw new BadRequestException(MessageConstant.ORDER.ORDER_NOT_FOUND);
        else if (!isOrderValid)
            throw new BadRequestException(MessageConstant.ORDER.ORDER_DELETED);
        else {
            orderItemEntityRepository.deleteByOrderEntity_Id(id);
            orderRepository.deleteById(id);
        }
        return response(MessageConstant.SUCCESSFULLY);
    }

    public StructureRS paymentMethod(PaymentStatusRQ paymentStatusRQ, Long id) {
        if (!orderRepository.existByIds(id))
            throw new BadRequestException(MessageConstant.ORDER.ORDER_NOT_FOUND);
        if (paymentStatusRQ.getPaymentMethod() == OrderPaymentMethodEnum.Cash ||
                paymentStatusRQ.getPaymentMethod() == OrderPaymentMethodEnum.Bank
        ) {
            Optional<OrderEntity> orderEntityOptional = orderRepository.findByIdAndDeletedAtIsNull(id);
            OrderEntity orderEntity = orderEntityOptional.get();
            orderEntity.setPaymentMethod(paymentStatusRQ.getPaymentMethod());
            orderRepository.save(orderEntity);
        } else {
            throw new BadRequestException(MessageConstant.ORDER.PAYMENT_CANNOT_UPDATE);
        }
        return response(MessageConstant.ORDER.ORDER_UPDATED);
    }

    public StructureRS orderStatus(OrderStatusRQ orderStatusRQ, Long id) {
        if (!orderRepository.existByIds(id))
            throw new BadRequestException(MessageConstant.ORDER.ORDER_NOT_FOUND);
        if (orderStatusRQ.getStatus() == OrderStatusEnum.Prepare ||
                orderStatusRQ.getStatus() == OrderStatusEnum.Cooking ||
                orderStatusRQ.getStatus() == OrderStatusEnum.Cancel ||
                orderStatusRQ.getStatus() == OrderStatusEnum.Complete
        ) {
            Optional<OrderEntity> orderEntityOptional = orderRepository.findByIdAndDeletedAtIsNull(id);
            OrderEntity orderEntity = orderEntityOptional.get();
            orderEntity.setStatus(orderStatusRQ.getStatus());
            orderRepository.save(orderEntity);
        } else {
            throw new BadRequestException(MessageConstant.ORDER.STATUS_CANNOT_UPDATE);
        }
        return response(MessageConstant.ORDER.ORDER_UPDATED);
    }
}