package com.rms.system.service;


import com.rms.system.base.BaseListingRQ;
import com.rms.system.base.BaseService;
import com.rms.system.base.StructureRS;
import com.rms.system.constant.MessageConstant;
import com.rms.system.db.entity.PermissionEntity;
import com.rms.system.db.entity.RoleEntity;
import com.rms.system.db.repository.PermissionRepository;
import com.rms.system.db.repository.RoleRepository;
import com.rms.system.exception.httpstatus.BadRequestException;
import com.rms.system.model.projection.role.RoleEntityInfo;
import com.rms.system.model.request.role.PermissionRQ;
import com.rms.system.model.request.role.RoleRQ;
import com.rms.system.model.request.role.UpdatePermissionRQ;
import com.rms.system.model.respond.role.PermissionRS;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class RoleService extends BaseService {

    private final RoleRepository roleRepository;
    private final PermissionRepository permissionRepository;

    public StructureRS getRoles(BaseListingRQ request) {
        Page<RoleEntityInfo> roleEntityPage = roleRepository.findByNameStartsWithAndCode(request.getQuery(), request.getPageable(request.getSort(), request.getOrder()));
        return response(roleEntityPage.getContent(), roleEntityPage);
    }

    public StructureRS createRole(RoleRQ roleRQ) {

        if (roleRepository.existsByName(roleRQ.getName()))
            throw new BadRequestException(MessageConstant.ROLE.ROLE_NAME_EXISTS);

        if (roleRepository.existsByCode(roleRQ.getCode()))
            throw new BadRequestException(MessageConstant.ROLE.ROLE_CODE_EXISTS);

        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setName(roleRQ.getName());
        roleEntity.setCode(roleRQ.getCode());
        roleRepository.save(roleEntity);
        return response(MessageConstant.ROLE.ROLE_CREATED_SUCCESSFULLY);
    }

    public StructureRS updateRole(RoleRQ roleRQ, Long id) {
        Optional<RoleEntity> roleEntity = roleRepository.findById(id);

        if (roleRepository.existsByNameEqualsAndIdNotEquals(roleRQ.getName(), id))
            throw new BadRequestException(MessageConstant.ROLE.ROLE_NAME_EXISTS);

        if (roleRepository.existsByIdAndDeletedAtIsNotNull(id))
            throw new BadRequestException(MessageConstant.ROLE.ROLE_NOT_FOUND);

        if (roleRepository.existsByCodeEqualsAndIdNotEquals(roleRQ.getCode(), id))
            throw new BadRequestException(MessageConstant.ROLE.ROLE_CODE_EXISTS);
        if (roleEntity.isEmpty())
            throw new BadRequestException(MessageConstant.ROLE.ROLE_NOT_FOUND);

        RoleEntity role = roleEntity.get();
        role.setName(roleRQ.getName());
        role.setCode(roleRQ.getCode());
        roleRepository.save(role);
        return response(MessageConstant.ROLE.ROLE_IS_UPDATED);
    }

    public StructureRS updatePermission(UpdatePermissionRQ updatePermissionRQ) {

        if (!roleRepository.existsById(updatePermissionRQ.getRoleId()))
            throw new BadRequestException(MessageConstant.ROLE.ROLE_NOT_FOUND);

        if (roleRepository.existsByIdAndDeletedAtIsNotNull(updatePermissionRQ.getRoleId()))
            throw new BadRequestException(MessageConstant.ROLE.ROLE_NOT_FOUND);

        RoleEntity roleEntity = roleRepository.findByIdFetchPermission(updatePermissionRQ.getRoleId());

        List<Long> list = updatePermissionRQ.getPermissions().stream().map(PermissionRQ::getPermissionId).toList();

        for (Long id : list) {
            if (!permissionRepository.existsById(id))
                throw new BadRequestException(MessageConstant.PERMISSION.NOT_FOUNT);
        }

        List<PermissionEntity> permissionEntities = permissionRepository.findByIdIn(list);


        for (PermissionRQ permissionRQ : updatePermissionRQ.getPermissions()) {
            Optional<PermissionEntity> optionalPermissionEntity = permissionEntities.stream().filter(it -> it.getId().equals(permissionRQ.getPermissionId())).findFirst();

            if (optionalPermissionEntity.isPresent()) {
                PermissionEntity permissionEntity = optionalPermissionEntity.get();

                if (permissionRQ.getStatus()) {
                    if (!roleEntity.getPermissionEntities().contains(permissionEntity))
                        roleEntity.getPermissionEntities().add(permissionEntity);
                } else
                    roleEntity.getPermissionEntities().remove(permissionEntity);
            }
        }
        roleRepository.save(roleEntity);

        return response(MessageConstant.ROLE.ROLE_IS_UPDATED);
    }

    public StructureRS getRoleById(Long id) {

        Optional<RoleEntity> roleEntity = roleRepository.findByIdFetchPermissionInfo(id);
        Optional<RoleEntityInfo> roleEntityInfo = roleRepository.findRoleEntityInfoByIdEquals(id);
        List<PermissionEntity> permissionEntities = permissionRepository.findAll();

        if (roleEntity.isEmpty())
            throw new BadRequestException(MessageConstant.ROLE.ROLE_NOT_FOUND);

        List<PermissionRS> listPermission = new ArrayList<>();
        Map<String, Object> respond = new HashMap<>();

        for (PermissionEntity per : permissionEntities) {
            Integer status = roleEntity.get().getPermissionEntities().stream()
                    .map(PermissionEntity::getId)
                    .anyMatch(p -> p.equals(per.getId())) ? 1 : 0;

            PermissionRS permissionRS = new PermissionRS();
            permissionRS.setId(per.getId());
            permissionRS.setName(per.getName());
            permissionRS.setModule(per.getModule());
            permissionRS.setStatus(status);
            listPermission.add(permissionRS);
        }
        respond.put("role", roleEntityInfo);
        respond.put("permissions", listPermission);

        return response(respond);
    }

    public StructureRS deleteRoleById(Long id) {
        Optional<RoleEntity> roleEntity = roleRepository.findById(id);

        if (roleEntity.isEmpty())
            throw new BadRequestException(MessageConstant.ROLE.ROLE_NOT_FOUND);

        roleRepository.deleteById(roleEntity.get().getId());
        return response(MessageConstant.ROLE.ROLE_DELETED_SUCCESSFULLY);
    }

}
