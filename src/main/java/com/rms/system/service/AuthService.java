package com.rms.system.service;

import com.rms.system.base.BaseService;
import com.rms.system.base.StructureRS;
import com.rms.system.constant.MessageConstant;
import com.rms.system.db.entity.UserEntity;
import com.rms.system.db.entity.enumentity.UserGenderEnum;
import com.rms.system.db.repository.RoleRepository;
import com.rms.system.db.repository.UserRepository;
import com.rms.system.exception.httpstatus.BadRequestException;
import com.rms.system.model.request.auth.LoginRQ;
import com.rms.system.model.request.auth.RegisterRQ;
import com.rms.system.security.UserPrincipal;
import com.rms.system.utils.TokenUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Sombath
 * create at 26/1/24 2:08 AM
 */

@Service
@RequiredArgsConstructor
public class AuthService extends BaseService {

    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final TokenUtils tokenUtils;
    private final PasswordEncoder passwordEncoder;

    public StructureRS login(LoginRQ request) {

        System.out.println(passwordEncoder.encode(request.getPassword()));

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword());
        Authentication auth = authenticationManager.authenticate(authenticationToken);

        UserPrincipal userPrincipal = (UserPrincipal) auth.getPrincipal();

        if (!userPrincipal.getStatus())
            throw new BadRequestException(MessageConstant.AUTH.ACCOUNT_DEACTIVATE);

        Map<String, Object> respond = new HashMap<>();

        respond.put("user", userPrincipal);
        respond.put("token", tokenUtils.generateToken(userPrincipal));

        return response(respond);
    }


    public StructureRS register(RegisterRQ registerRQ) {

        userService.register(registerRQ);

        return response(MessageConstant.USER.USER_IS_CREATED);
    }
}
