package com.rms.system.service;

import com.rms.system.base.BaseListingRQ;
import com.rms.system.base.BaseService;
import com.rms.system.base.RangeTimeRQ;
import com.rms.system.base.StructureRS;
import com.rms.system.constant.MessageConstant;
import com.rms.system.db.repository.FoodReportRepository;
import com.rms.system.exception.httpstatus.BadRequestException;
import com.rms.system.model.projection.report.FoodReportInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class FoodReportService extends BaseService {
    private final FoodReportRepository foodReportRepository;

    public StructureRS getFood(String startText, String endText, String monthText,Integer top, RangeTimeRQ timeReport, BaseListingRQ request) {
        if (startText != null && endText == null) {
            Date date = timeReport.getRang(startText);
            Page<FoodReportInfo> byDay = foodReportRepository.findFoodDaySold(date, request.getQuery(), request.getPageable(request.getSort(), request.getOrder()));
            return response(byDay.getContent(), byDay);
        }else if(startText != null){
            Date startDate = timeReport.getRang(startText);
            Date endDate = timeReport.getRang(endText);
            if (startDate.after(endDate)){
                throw new BadRequestException(MessageConstant.REPORT_FOOD.INVALID_END_DATE);
            }
            Page<FoodReportInfo> foodReportInfos =  foodReportRepository.findFoodBetweenDay(startDate,endDate,request.getQuery(),request.getTopPageable("totalQuantitySold",top));
            return response(foodReportInfos.getContent(), foodReportInfos);
        }else if (monthText != null) {
            Date month = timeReport.getRang(monthText);
            Page<FoodReportInfo> topFood = foodReportRepository.findFoodMonthSold(month,request.getQuery(),request.getTopPageable("totalQuantitySold",top));
            return response(topFood.getContent(), topFood);
        }else{
            String dateText = timeReport.getDate();
            Date date = timeReport.getRang(dateText);
            Page<FoodReportInfo> byTimeNow = foodReportRepository.findFoodDaySold(date, request.getQuery(), request.getPageable(request.getSort(), request.getOrder()));
            return response(byTimeNow.getContent(), byTimeNow);
        }
    }

}