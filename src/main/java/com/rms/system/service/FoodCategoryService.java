package com.rms.system.service;

import com.rms.system.base.BaseListingRQ;
import com.rms.system.base.BaseService;
import com.rms.system.base.StructureRS;
import com.rms.system.constant.MessageConstant;
import com.rms.system.db.entity.FoodCategoryEntity;
import com.rms.system.db.entity.FoodEntity;
import com.rms.system.db.repository.FoodCategoryEntityRepository;
import com.rms.system.db.repository.FoodEntityRepository;
import com.rms.system.exception.httpstatus.BadRequestException;
import com.rms.system.model.projection.food.FoodCategoryEntityInfo;
import com.rms.system.model.request.food.FoodCategoryRQ;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class FoodCategoryService extends BaseService {
    private final FoodCategoryEntityRepository foodCategoryEntityRepository;
    private final FoodEntityRepository foodEntityRepository;

    public StructureRS getCategory(BaseListingRQ request) {
        Page<FoodCategoryEntityInfo> foodCategoryEntityInfos = foodCategoryEntityRepository.findByNameLike(
                request.getQuery(),request.getPageable(request.getSort(),request.getOrder()));
        return response(foodCategoryEntityInfos.getContent(), foodCategoryEntityInfos);
    }

    public void addCategory(FoodCategoryRQ foodCategoryRQ) {
        if(foodCategoryEntityRepository.existsByNameLike(foodCategoryRQ.getName()))
            throw new BadRequestException(MessageConstant.CATEGORY.CATE_NAME_EXISTS);

        FoodCategoryEntity foodCategoryEntity = new FoodCategoryEntity();
        foodCategoryEntity.setName(foodCategoryRQ.getName());
        foodCategoryEntityRepository.save(foodCategoryEntity);
    }

    public void updateCategory(FoodCategoryRQ foodCategoryRQ, Long id) {
        Optional<FoodCategoryEntity> foodCategoryEntities = foodCategoryEntityRepository.findByIdAndDeletedAtIsNull(id);
        if (foodCategoryEntities.isEmpty()){
            throw new BadRequestException(MessageConstant.CATEGORY.Category_NOT_FOUND);
        }
        FoodCategoryEntity foodCategoryEntity = foodCategoryEntities.get();
        foodCategoryEntity.setName(foodCategoryRQ.getName());
        foodCategoryEntityRepository.save(foodCategoryEntity);
    }

    public void deleteCategory(Long id) {
        Optional<FoodCategoryEntity> foodCategoryEntities = foodCategoryEntityRepository.findByIdAndDeletedAtIsNull(id);

        if(foodCategoryEntities.isEmpty()){
            throw new BadRequestException(MessageConstant.CATEGORY.Category_NOT_FOUND);
        }
        List<FoodEntity> foodEntities = foodEntityRepository.findByFoodCategoryEntityIdEquals(id);
        if (!foodEntities.isEmpty())
            throw new BadRequestException(MessageConstant.CATEGORY.ASSOCIATE_WITH_FOOD);

        foodCategoryEntityRepository.deleteById(id);
    }

    public StructureRS getId(Long id) {
        if (!foodCategoryEntityRepository.existsById(id))
            throw new BadRequestException(MessageConstant.CATEGORY.Category_NOT_FOUND);

        FoodCategoryEntityInfo foodCategoryEntity = foodCategoryEntityRepository.findOneById(id);
        return response(foodCategoryEntity);
    }
}
