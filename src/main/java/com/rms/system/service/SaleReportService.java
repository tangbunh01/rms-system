package com.rms.system.service;

import com.rms.system.base.BaseListingRQ;
import com.rms.system.base.BaseService;
import com.rms.system.base.RangeTimeRQ;
import com.rms.system.base.StructureRS;
import com.rms.system.constant.MessageConstant;
import com.rms.system.db.entity.enumentity.OrderPaymentMethodEnum;
import com.rms.system.db.repository.SaleReportRepository;
import com.rms.system.exception.httpstatus.BadRequestException;
import com.rms.system.model.projection.report.IncomeReportInfo;
import com.rms.system.model.projection.report.SaleReportInfo;
import com.rms.system.model.request.role.RoleRQ;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class SaleReportService extends BaseService {
    private final SaleReportRepository saleReportRepository;

    public StructureRS getIncomeSold(String startText, String endText, String monthText, String type,OrderPaymentMethodEnum payment,RoleRQ roleRQ, RangeTimeRQ timeReport, BaseListingRQ request) {
        if (startText != null && endText == null){
            Date date = timeReport.getRang(startText);
            if(payment == null) {
                List<IncomeReportInfo> incomeDay = saleReportRepository.findIncomeByDayOrNow(date);
                return response(incomeDay);
            }else {
                List<IncomeReportInfo> incomeHasPayment = saleReportRepository.findIncomeByDayHasPayment(date, payment);
                return response(incomeHasPayment);
            }
        }
        else if (startText != null){
            Date startdate = timeReport.getRang(startText);
            Date endDate = timeReport.getRang(endText);
            if(startdate.after(endDate)){throw new BadRequestException(MessageConstant.REPORT_FOOD.INVALID_END_DATE);}
            if(payment == null) {
                if (Objects.equals(type,"list")){
                    Page<IncomeReportInfo> incomeBetweenDateList = saleReportRepository.findIncomeBetweenDateList(startdate, endDate, request.getPageable(request.getSort(),request.getOrder()));
                    return response(incomeBetweenDateList.getContent(), incomeBetweenDateList);
                }
                List<IncomeReportInfo> incomeBetweenDate = saleReportRepository.findIncomeBetweenDate(startdate, endDate);
                return response(incomeBetweenDate);
            }
            else {
                List<IncomeReportInfo> incomeBetweenDateHasPayment = saleReportRepository.findIncomeBetweenDateHasPayment(startdate,endDate,payment);
                return response(incomeBetweenDateHasPayment);
            }
        }
        else if (monthText != null) {
            Date month = timeReport.getRang(monthText);
            if(payment != null) {
                List<IncomeReportInfo> incomeByMonthPayment = saleReportRepository.findIncomeByMonth(month, payment);
                return response(incomeByMonthPayment);
            }else {
                if(Objects.equals(type,"list")){
                    Page<IncomeReportInfo> incomeMonthList = saleReportRepository.findIncomeMonthList(month, request.getPageable(request.getSort(),request.getOrder()));
                    return response(incomeMonthList.getContent(), incomeMonthList);
                }
                List<IncomeReportInfo> incomeByMonth = saleReportRepository.findIncomeByMonthNonePayment(month);
                return response(incomeByMonth);
            }
        }
        else {
            String dateNow = timeReport.getDate();
            Date date = timeReport.getRang(dateNow);
            if (payment == null) {
                List<IncomeReportInfo> incomeNow = saleReportRepository.findIncomeByDayOrNow(date);
                return response(incomeNow);
            }else {
                List<IncomeReportInfo> incomeHasPayment = saleReportRepository.findIncomeByDayHasPayment(date,payment);
                return response(incomeHasPayment);
            }
        }
    }
    public StructureRS getStaffSold(String startText, String endText,String monthText, RangeTimeRQ timeReport, BaseListingRQ request) {
        if (startText != null && endText == null){
            Date date = timeReport.getRang(startText);
            Page<SaleReportInfo> incomeDay = saleReportRepository.findStaffSoldByDay(date,request.getQuery(),request.getPageable());
            return response(incomeDay.getContent(), incomeDay);
        }
        else if (startText != null){
            Date startdate = timeReport.getRang(startText);
            Date endDate = timeReport.getRang(endText);
            if (startdate.after(endDate)){
                throw new BadRequestException(MessageConstant.REPORT_FOOD.INVALID_END_DATE);
            }
            Page<SaleReportInfo> incomeBetweenDate = saleReportRepository.findStaffSoldBetweenDate(startdate, endDate, request.getQuery(), request.getPageable(request.getSort(),request.getOrder()));
            return response(incomeBetweenDate.getContent(),incomeBetweenDate);
        }
        else if (monthText != null){
            Date month = timeReport.getRang(monthText);
            Page<SaleReportInfo> incomeByMonth = saleReportRepository.getStaffByMonth(month,request.getQuery(),request.getPageable(request.getSort(), request.getOrder()));
            return response(incomeByMonth.getContent(), incomeByMonth);
        }
        else {
            String dateText = timeReport.getDate();
            Date date = timeReport.getRang(dateText);
            Page<SaleReportInfo> incomeNow = saleReportRepository.findStaffSoldByDay(date, request.getQuery(), request.getPageable(request.getSort(),request.getOrder()));
            return response(incomeNow.getContent(), incomeNow);
        }
    }

}
