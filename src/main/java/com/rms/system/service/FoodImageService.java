package com.rms.system.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.rms.system.awsS3.S3Buckets;
import com.rms.system.awsS3.S3Service;
import com.rms.system.base.BaseService;
import com.rms.system.base.StructureRS;
import com.rms.system.constant.MessageConstant;
import com.rms.system.db.entity.FoodEntity;
import com.rms.system.db.entity.FoodImageEntity;
import com.rms.system.db.repository.FoodEntityRepository;
import com.rms.system.db.repository.FoodImageEntityRepository;
import com.rms.system.exception.httpstatus.BadRequestException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class FoodImageService extends BaseService {

    private final FoodImageEntityRepository imageFoodRepository;
    private final FoodEntityRepository foodEntityRepository;
    private final S3Service s3Service;
    private final S3Buckets s3Buckets;
    private final AmazonS3 s3client;

    public void uploadFileTos3bucket(String fileName, File file) {
        s3client.putObject(new PutObjectRequest(s3Buckets.getName(), fileName, file)
                .withCannedAcl(CannedAccessControlList.PublicRead));
    }
    @Transactional
    public StructureRS uploadFoodImageById(Long foodId, MultipartFile files) {
        Optional<FoodEntity> foodImage = foodEntityRepository.findById(foodId);

        if (foodImage.isEmpty())
            throw new BadRequestException(MessageConstant.FOOD.FOOD_NOT_FOUND);
        if (files == null)
            throw new BadRequestException(MessageConstant.FOOD.IMAGE_NOT_FOUND);

        if (
                !files.getContentType().equals("image/jpeg") &&
                        !files.getContentType().equals("image/jpg") &&
                        !files.getContentType().equals("image/png") &&
                        !files.getContentType().equals("image/webp")
        )
            throw new BadRequestException(MessageConstant.FOOD.IMAGE_INVALID);
        if (files.getSize() > 1024 * 1024 * 10)
            throw new BadRequestException(MessageConstant.FOOD.IMAGE_TOO_LARGE);

        try {
            String fileUrl = "";
            File image = s3Service.convertMultiPartToFile(files);
            String fileName = "food-image/" + foodId + s3Service.generateFileName(files);
            fileUrl = s3Buckets.getEndPointUrl() + "/" + s3Buckets.getName() + "/" + fileName;

            uploadFileTos3bucket(fileName, image);
            image.delete();

            FoodEntity foodEntity = foodImage.get();
            foodEntity.setFoodImage(fileUrl);
            foodEntityRepository.save(foodEntity);

            FoodImageEntity foodImageEntity = new FoodImageEntity();
            foodImageEntity.setFoodEntity(foodEntityRepository.getReferenceById(foodImage.get().getId()));
            foodImageEntity.setUrl(fileUrl);


            imageFoodRepository.save(foodImageEntity);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        return response();
    }

    public StructureRS deleteFoodImage(Long foodId) {
        Optional<FoodEntity> optionalFoodEntity = foodEntityRepository.findById(foodId);

        if (optionalFoodEntity.isEmpty())
            throw new BadRequestException(MessageConstant.FOOD.FOOD_NOT_FOUND);
        List<FoodImageEntity> optionalFoodImageEntity = imageFoodRepository.findByFoodImageEntityIdEquals(foodId);
        if (optionalFoodImageEntity.isEmpty()){
            throw new BadRequestException(MessageConstant.FOOD.IMAGE_NOT_FOUND);
        }


        for (FoodImageEntity foodImage : optionalFoodImageEntity)
            imageFoodRepository.deleteById(foodImage.getId());

        FoodEntity foodEntity = optionalFoodEntity.get();
        foodEntity.setFoodImage(null);
        foodEntityRepository.save(foodEntity);
        return response(MessageConstant.FOOD.IMAGE_DELETE);
    }

    public StructureRS updateFoodImageByFoodId(MultipartFile file, Long foodId) {
        Optional<FoodEntity> optionalFoodEntity = foodEntityRepository.findById(foodId);
        if (optionalFoodEntity.isEmpty())
            throw new BadRequestException(MessageConstant.FOOD.FOOD_NOT_FOUND);

        List<FoodImageEntity> optionalFoodImageEntity = imageFoodRepository.findByFoodImageEntityIdEquals(foodId);
        if (optionalFoodImageEntity.isEmpty()){
            throw  new BadRequestException(MessageConstant.FOOD.IMAGE_NOT_FOUND_IN_FOOD);
        }
        if (file == null)
            throw new BadRequestException(MessageConstant.FOOD.IMAGE_NOT_FOUND);
        // Validate file type and size
        if(
                !file.getContentType().equals("image/jpeg")
                        && !file.getContentType().equals("image/jpg")
                        && !file.getContentType().equals("image/png")
                        && !file.getContentType().equals("image/webp")
        )
            throw new BadRequestException(MessageConstant.FOOD.IMAGE_INVALID);
        if (file.getSize() > 1024 * 1024 * 10)
            throw new BadRequestException(MessageConstant.FOOD.IMAGE_TOO_LARGE);

        try {
            for (FoodImageEntity foodImage : optionalFoodImageEntity){
                String oldImageUrl = foodImage.getUrl();
                String oldFileName = oldImageUrl.substring(oldImageUrl.lastIndexOf("/") + 1);
                DeleteObjectRequest deleteRequest = new DeleteObjectRequest(s3Buckets.getName(), "food-image/" + oldFileName);
                s3client.deleteObject(deleteRequest);

                String newFileUrl = "";
                File image = s3Service.convertMultiPartToFile(file);
                String newFileName = "food-image/" + foodImage.getFoodEntity().getId() + s3Service.generateFileName(file);
                newFileUrl = s3Buckets.getEndPointUrl() + "/" + s3Buckets.getName() + "/" + newFileName;

                uploadFileTos3bucket(newFileName, image);
                image.delete();// Delete the existing image file from S3 bucket
                foodImage.setUrl(newFileUrl);
                imageFoodRepository.save(foodImage);
            }
            if(!optionalFoodImageEntity.isEmpty()){
                FoodImageEntity foodImage = optionalFoodImageEntity.get(optionalFoodImageEntity.size() - 1);
                FoodEntity foodEntity = optionalFoodEntity.get();
                foodEntity.setFoodImage(foodImage.getUrl());
                foodEntityRepository.save(foodEntity);
            }

        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        return response();
    }
}


