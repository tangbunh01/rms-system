package com.rms.system.model.request.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.rms.system.exception.anotation.FieldsValueMatch;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
@FieldsValueMatch(field = "password", fieldMatch = "confirmPassword", message = "Password is not match")
public class PasswordResetRQ {

    @NotBlank(message = "Old password field is required")
    @JsonProperty("old_password")
    private String oldPassword;

    @NotBlank(message = "Password field is required")
    private String password;

    @NotBlank(message = "Confirm password is required")
    @JsonProperty("confirm_password")
    private String confirmPassword;
}