package com.rms.system.model.request.order;

import com.rms.system.db.entity.enumentity.OrderPaymentMethodEnum;
import jakarta.validation.constraints.NotNull;
import lombok.Data;


@Data
public class PaymentStatusRQ {
    @NotNull(message= "Please provide payment method")
    private OrderPaymentMethodEnum paymentMethod;

}
