package com.rms.system.model.request.auth;


import jakarta.validation.constraints.NotBlank;
import lombok.Data;

/**
 * @author Sombath
 * create at 11/9/23 1:48 PM
 */

@Data
public class LoginRQ {

    @NotBlank(message = "Please provide a username, email, phone")
    private String username;

    @NotBlank(message = "Please provide a password")
    private String password;
}
