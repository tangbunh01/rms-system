package com.rms.system.model.request.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class OrderItemRQ{

    @NotNull(message = "please provide your food_id")
    @JsonProperty("food_Id")
    private  Long foodId;

    @NotNull(message = "please provide your qty")
    @Min(1)
    private Integer quantity;
}
