package com.rms.system.model.request.food;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
@Data
public class FoodRQ {
    @NotBlank(message = "please provide your name")
    private String name;

    @NotBlank(message = "please provide your code")
    @Size(min = 1,max = 20)
    private String code;

    @Min(0)
    private Double price;

    @Min(0)
    private Double discount;

    @NotBlank(message = "please provide your description")
    @Size(min = 1, max = 255)
    private String description;

    private Boolean status;

    @NotNull
    @JsonProperty("food_categoryId")
    private Long foodCategoryId;
}
