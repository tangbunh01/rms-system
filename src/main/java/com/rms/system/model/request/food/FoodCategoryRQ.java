package com.rms.system.model.request.food;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class FoodCategoryRQ {

    @NotBlank(message = "please provide a name")
    private String name;
}
