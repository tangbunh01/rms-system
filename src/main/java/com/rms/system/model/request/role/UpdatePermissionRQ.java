package com.rms.system.model.request.role;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

@Data
public class UpdatePermissionRQ {

    @NotNull
    @JsonProperty("role_id")
    private Long roleId;

    @NotEmpty
    private List<@Valid PermissionRQ> permissions;
}
