package com.rms.system.model.request.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class UpdateUserRQ {

    private String name;

    @NotBlank(message = "Gender field is required")
    private String gender;

    @NotBlank(message = "Phone field is required")
    private String phone;

    private String avatar;

    @NotNull(message = "Role is required")
    @JsonProperty("role_id")
    private Long roleId;
}
