package com.rms.system.model.request.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

@Data
public class OrderRQ {
    @Min(value = 1, message = "Id must be greater than or equal to 1")
    @JsonProperty("table_Id")
    private Long tableId;
    @Min(value = 1, message = "Id must be greater than or equal to 1")
    @JsonProperty("user_Id")
    private Long userId;
    @NotNull
    private List<@Valid OrderItemRQ> items;
}
