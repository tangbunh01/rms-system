package com.rms.system.model.request.table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.rms.system.db.entity.enumentity.TableStatusEnum;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.Getter;

@Data
public class TableRQ {
    @NotBlank(message = "Please provide a name for the table")
    private String name;
    @NotBlank(message = "Please provide the number of seats for the table")
    @JsonProperty("seat_Capacity")
    private String seatCapacity;
    @NotNull(message = "Please select the table status")
    private TableStatusEnum status;
}
