package com.rms.system.model.request.order;

import com.rms.system.db.entity.enumentity.OrderStatusEnum;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class OrderStatusRQ {
    @NotNull(message="Please provide order status")
    private OrderStatusEnum status;
}
