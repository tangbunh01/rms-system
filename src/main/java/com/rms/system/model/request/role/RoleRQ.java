package com.rms.system.model.request.role;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

/**
 * @author Sombath
 * create at 24/1/24 4:02 PM
 */

@Data
public class RoleRQ {

    @NotBlank(message = "Please provide a name")
    private String name;
    @NotBlank(message = "Please provide a code")
    private String code;

}
