package com.rms.system.model.request.user;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.rms.system.db.entity.RoleEntity;
import com.rms.system.exception.anotation.FieldsValueMatch;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.time.Instant;
import java.time.LocalDateTime;

@Data
@FieldsValueMatch(field = "password", fieldMatch = "confirmPassword", message = "Password and confirm password is not match")
public class UserRQ {

    private String name;
    @NotBlank(message = "Username field is required")
    private String username;

    @NotBlank(message = "Gender field is required")
    private String gender;

    @Email
    @NotBlank(message = "Email field is required")
    private String email;

    @NotBlank(message = "Password field is required")
    private String password;

    @NotBlank(message = "Confirm password field is required")
    @JsonProperty("confirm_password")
    private String confirmPassword;

    @NotBlank(message = "Phone field is required")
    private String phone;

    @NotNull(message = "Salary field is required")
    private Double salary;

    @NotNull(message = "Hire date field is required")
    @JsonProperty("hire_date")
    private Instant hireDate;

    private String avatar;

    private Boolean status;

    @NotNull(message = "Role is required")
    @JsonProperty("role_id")
    private Long roleId;

}
