package com.rms.system.model.request.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.rms.system.exception.anotation.FieldsValueMatch;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.Instant;

@Data
@FieldsValueMatch(field = "password", fieldMatch = "confirmPassword")
public class RegisterRQ {

    private String name;
    @NotBlank(message = "Username field is required")
    private String username;

    @NotBlank(message = "Gender field is required")
    private String gender;

    @Email
    @NotBlank(message = "Email field is required")
    private String email;

    @NotBlank(message = "Password field is required")
    private String password;

    @NotBlank(message = "Confirm password field is required")
    @JsonProperty("confirm_password")
    private String confirmPassword;

    @NotBlank(message = "Phone field is required")
    private String phone;

    private Boolean status;

}
