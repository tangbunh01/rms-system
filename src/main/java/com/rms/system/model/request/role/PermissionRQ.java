package com.rms.system.model.request.role;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class PermissionRQ {

    @NotNull
    @JsonProperty("permission_id")
    private Long permissionId;

    @NotNull
    private Boolean status;
}
