package com.rms.system.model.request.user;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class UpdateUserCustomer {

    private String name;

    private String username;

    @Email
    private String email;

    @NotBlank(message = "Gender field is required")
    private String gender;

    @NotBlank(message = "Phone field is required")
    private String phone;

    private String avatar;
}
