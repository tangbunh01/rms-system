package com.rms.system.model.respond.role;

import lombok.Data;

@Data
public class PermissionRS {
    private Long id;
    private String name;
    private String module;
    private Integer status;
}
