package com.rms.system.model.projection.report;

/**
 * Projection for {@link com.rms.system.db.entity.OrderItemEntity}
 */
public interface FoodReportInfo {
    Double getUnitPrice();
    Integer getTotalQuantitySold();
    FoodEntityInfo getFood();
    Double getTotalPrice();
    /**
     * Projection for {@link com.rms.system.db.entity.FoodEntity}
     */

    interface FoodEntityInfo {
        String getName();
        Long getId();
        String getCode();
    }

}