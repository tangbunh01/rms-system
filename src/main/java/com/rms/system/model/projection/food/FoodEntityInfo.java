package com.rms.system.model.projection.food;

import com.rms.system.db.entity.FoodImageEntity;
import java.time.Instant;
import java.util.List;

/**
 * Projection for {@link com.rms.system.db.entity.FoodEntity}
 */
public interface FoodEntityInfo {
    Long getId();

    String getName();

    String getCode();

    Double getPrice();

    Double getDiscount();

    String getDescription();

    Boolean getStatus();

    FoodCategoryEntityInfo getFoodCategoryEntity();

    String getFoodImage();
    /**
     * Projection for {@link com.rms.system.db.entity.FoodCategoryEntity}
     */
    interface FoodCategoryEntityInfo {
        String getName();
        Long getId();
    }


}