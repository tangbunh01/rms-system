package com.rms.system.model.projection.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.rms.system.db.entity.enumentity.OrderPaymentMethodEnum;
import com.rms.system.db.entity.enumentity.OrderStatusEnum;

import java.time.Instant;
import java.util.List;

/**
 * Projection for {@link com.rms.system.db.entity.OrderEntity}
 */
public interface OrderEntityInfoById {
    Long getId();

    OrderPaymentMethodEnum getPaymentMethod();

    Double getTotalPrice();

    OrderStatusEnum getStatus();
    @JsonProperty("orders")
    List<OrderItemEntityInfo1> getOrderItemEntities();

    /**
     * Projection for {@link com.rms.system.db.entity.OrderItemEntity}
     */
    interface OrderItemEntityInfo1 {
        @JsonProperty("orderItem_id")
        Long getId();

        Integer getQuantity();

        Double getTotalPrice();

        FoodEntityInfo getFoodEntity();

        /**
         * Projection for {@link com.rms.system.db.entity.FoodEntity}
         */
        interface FoodEntityInfo {
            @JsonProperty("food_id")
            Long getId();

            String getName();

            String getCode();

            String getFoodImage();

            Double getPrice();

            Double getDiscount();

            List<FoodImageEntityInfo> getFoodImageEntities();

            /**
             * Projection for {@link com.rms.system.db.entity.FoodImageEntity}
             */
            interface FoodImageEntityInfo {
                Long getId();

                String getUrl();
            }
        }
    }
}