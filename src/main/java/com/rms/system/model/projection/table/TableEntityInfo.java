package com.rms.system.model.projection.table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.rms.system.db.entity.enumentity.TableStatusEnum;

/**
 * Projection for {@link com.rms.system.db.entity.TableEntity}
 */
public interface TableEntityInfo {
    Long getId();

    String getName();
    @JsonProperty("seat_Capacity")
    String getSeatCapacity();

    TableStatusEnum getStatus();
    interface UserEntityInfo {
        String getName();
    }
    interface UserEntityInfo1 {
        String getName();
    }
}