package com.rms.system.model.projection.order;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.rms.system.db.entity.OrderEntity;
import com.rms.system.db.entity.TableEntity;
import com.rms.system.db.entity.UserEntity;
import com.rms.system.db.entity.enumentity.OrderPaymentMethodEnum;
import com.rms.system.db.entity.enumentity.OrderStatusEnum;
import com.rms.system.db.entity.enumentity.TableStatusEnum;
import com.rms.system.db.entity.enumentity.UserGenderEnum;

import java.time.Instant;

/**
 * Projection for {@link OrderEntity}
 */
public interface OrderEntityInfo {
    Long getId();
    @JsonProperty("total_Price")
    Double getTotalPrice();
    @JsonProperty("user")
    UserEntityInfo1 getUserEntity();
    @JsonProperty("table")
    TableEntityInfo1 getTableEntity();

    /**
     * Projection for {@link com.rms.system.db.entity.UserEntity}
     */
    interface UserEntityInfo1 {
        Long getId();
        String getName();
    }

    /**
     * Projection for {@link com.rms.system.db.entity.TableEntity}
     */
    interface TableEntityInfo1 {
        Long getId();

        String getName();
    }
}