package com.rms.system.model.projection.report;

/**
 * Projection for {@link com.rms.system.db.entity.OrderEntity}
 */
public interface SaleReportInfo {
    //Long getId();

    OrderPaymentMethodEnum getPaymentMethod();

    Double getTotalPrice();
    UserEntityInfo getCashier();
    interface OrderPaymentMethodEnum {
        String getName();
    }
    interface TableEntity{
        String getName();
    }
    interface UserEntityInfo{
        String getName();
        String getUsername();
    }
}