package com.rms.system.model.projection.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.rms.system.db.entity.enumentity.UserGenderEnum;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.Instant;
import java.util.Date;

/**
 * Projection for {@link com.rms.system.db.entity.UserEntity}
 */
public interface UserEntityInfo {
    Long getId();

    String getName();

    String getUsername();

    UserGenderEnum getGender();

    String getEmail();

    String getPhone();

    Double getSalary();

//    @JsonFormat(pattern = "dd-MM-yyyy")
    Date getHireDate();

    String getAvatar();

    Boolean getStatus();

    @JsonProperty("role")
    RoleEntityInfo1 getRoleEntity();

    /**
     * Projection for {@link com.rms.system.db.entity.RoleEntity}
     */
    interface RoleEntityInfo1 {
        Long getId();
        String getName();
    }
}