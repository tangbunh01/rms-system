package com.rms.system.model.projection;

/**
 * Projection for {@link com.rms.system.db.entity.SupplierEntity}
 */
public interface SupplierEntityInfo {
    Long getId();

    String getName();

    String getEmail();

    String getPhone();

    UserEntityInfo getCreatedBy();

    UserEntityInfo getUpdateBy();

    /**
     * Projection for {@link com.rms.system.db.entity.UserEntity}
     */
    interface UserEntityInfo {
        String getUsername();
    }
}