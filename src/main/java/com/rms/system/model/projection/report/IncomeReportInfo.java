package com.rms.system.model.projection.report;

import com.rms.system.db.entity.enumentity.OrderPaymentMethodEnum;

import java.time.Instant;

/**
 * Projection for {@link com.rms.system.db.entity.OrderEntity}
 */
public interface IncomeReportInfo {
    Instant getIncomeDate();

    OrderPaymentMethodEnum getPaymentMethod();

    Double getTotalPrice();
}