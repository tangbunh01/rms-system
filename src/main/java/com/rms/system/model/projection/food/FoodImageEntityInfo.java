package com.rms.system.model.projection.food;

/**
 * Projection for {@link com.rms.system.db.entity.FoodImageEntity}
 */
public interface FoodImageEntityInfo {
    Long getId();

    String getUrl();
}