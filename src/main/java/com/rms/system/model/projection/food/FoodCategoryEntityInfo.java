package com.rms.system.model.projection.food;

import java.time.Instant;
import java.util.List;

/**
 * Projection for {@link com.rms.system.db.entity.FoodCategoryEntity}
 */
public interface FoodCategoryEntityInfo {
    Long getId();

    String getName();

    UserEntityInfo getCreatedBy();


    /**
     * Projection for {@link com.rms.system.db.entity.UserEntity}
     */
    interface UserEntityInfo {
        String getUsername();
        Long getId();
    }
}