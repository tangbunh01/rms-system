package com.rms.system.model.projection.role;

import java.time.Instant;

/**
 * Projection for {@link com.rms.system.db.entity.RoleEntity}
 */
public interface RoleEntityInfo {
    Long getId();

    String getName();

    String getCode();

    Instant getCreatedDate();

    Instant getUpdateDate();

    UserEntityInfo getCreatedBy();

    UserEntityInfo getUpdateBy();

    /**
     * Projection for {@link com.rms.system.db.entity.UserEntity}
     */
    interface UserEntityInfo {
        String getName();
    }
}