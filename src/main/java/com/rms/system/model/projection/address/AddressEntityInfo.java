package com.rms.system.model.projection.address;

import java.time.Instant;

/**
 * Projection for {@link com.rms.system.db.entity.AddressEntity}
 */
public interface AddressEntityInfo {
    Long getId();

    Instant getCreatedDate();

    Instant getUpdateDate();

    String getName();

    String getLocation();

    String getGoogleMapUrl();

    UserEntityInfo1 getCreatedBy();

    /**
     * Projection for {@link com.rms.system.db.entity.UserEntity}
     */
    interface UserEntityInfo1 {
        Long getId();

        String getName();
    }
}