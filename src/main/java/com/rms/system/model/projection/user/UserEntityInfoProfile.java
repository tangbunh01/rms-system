package com.rms.system.model.projection.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.rms.system.db.entity.enumentity.OrderPaymentMethodEnum;
import com.rms.system.db.entity.enumentity.OrderStatusEnum;
import com.rms.system.db.entity.enumentity.UserGenderEnum;

import java.time.Instant;
import java.util.List;

/**
 * Projection for {@link com.rms.system.db.entity.UserEntity}
 */
public interface UserEntityInfoProfile {
    Long getId();

    Instant getCreatedDate();

    Instant getUpdateDate();

    String getName();

    String getUsername();

    UserGenderEnum getGender();

    String getEmail();

    String getPhone();

    Double getSalary();

    Instant getHireDate();

    String getAvatar();

    Boolean getStatus();

    UserEntityInfo1 getCreatedBy();

    UserEntityInfo1 getUpdateBy();

    @JsonProperty("addresses")
    List<AddressEntityInfo1> getAddressEntities();

    /**
     * Projection for {@link com.rms.system.db.entity.UserEntity}
     */
    interface UserEntityInfo1 {
        Long getId();
        String getName();
    }

    /**
     * Projection for {@link com.rms.system.db.entity.AddressEntity}
     */
    interface AddressEntityInfo1 {

        String getName();

        String getLocation();

        String getGoogleMapUrl();
    }

}