package com.rms.system.constant;

import org.springframework.boot.convert.PeriodUnit;
import org.springframework.security.core.parameters.P;

/**
 * @author Sombath
 * create at 23/1/24 3:48 PM
 */
public class MessageConstant {

    public static final String SUCCESSFULLY = "Successfully";
    public static final String ALL = "ALL";
    public static class ADDRESS {
        public static final String ADDRESS_IS_CREATED = "Address has been created";
        public static final String ADDRESS_IS_EXISTED = "Address name is existed";
        public static final String ADDRESS_NOT_FOUND = "Address could not be fount";
        public static final String ADDRESS_IS_DELETED = "Address has been deleted";
    }
    public static class PERMISSION {
        public final static String NOT_FOUNT = "Permission could not be found";
    }

    public static class USER {
        public final static String USERNAME_IS_EXIST = "Username is exist";
        public static final String EMAIL_IS_EXIST = "Email is exist";
        public static final String USER_IS_CREATED = "User has been created";
        public static final String NOT_FOUND = "User could not be found";
        public static final String USER_IS_DELETED = "User has been deleted";
        public static final String USER_IS_UPDATE = "User has been updated";
        public static final String PROFILE_UPDATED = "Profile is updated";
        public static final String OLD_PASSWORD_WRONG = "Your old password is wrong";
        public static final String SAME_PASSWORD = "Your new password must be not the same your old password";
        public static final String PROFILE_IMAGE_UPLOADED = "Profile image has been uploaded";
        public static final String PROFILE_IMAGE_NOT_FOUND = "Profile image not found";
        public static final String PROFILE_IMAGE_NOT_VALID = "Profile image is not valid";
        public static final String PROFILE_IMAGE_TOO_LARGE = "Profile image is too large";
        public static final String ADMIN_USER = "Can't edit admin user";
    }

    public static class AUTH {
        public final static String INCORRECT_USERNAME_OR_PASSWORD = "Incorrect Username or password";
        public final static String ACCOUNT_DEACTIVATE = "Account have been deactivated";

        public final static String BAD_CREDENTIALS = "Bad credentials";
    }

    public static class ROLE {
        public final static String ADMIN = "ADMIN";
        public final static String USER = "USER";
        public static final String ROLE_CREATED_SUCCESSFULLY = "Role has been created";
        public static final String ROLE_NOT_FOUND = "Role could not be found";
        public static final String ROLE_DELETED_SUCCESSFULLY = "Role has been deleted";
        public static final String ROLE_NAME_EXISTS = "Role name has been exist";
        public static final String ROLE_IS_UPDATED = "Role has been updated";
        public static final String ROLE_CODE_EXISTS = "Role code has been exists";
    }
    public static  class SALE_REPORT{
        public final static String INPUT_DATE_MONTH = "please input date month.";
        public final static String INPUT_DATE = "please input date.";
    }
    public static class REPORT_FOOD{
        public final static String MAX_FOOD_NOT_FOUND = "Cannot find max food report";
        public final static String BETWEEN_DAY_REPORT = "Cannot report food between day";
        public final static String A_DAY_REPORT = "Cannot report a day";
        public final static String MONTH_REPORT = "Cannot report a month";
        public static final String INVALID_END_DATE = "Invalid date  provided.";
    }
    public static class ITEM_CATEGORY {
        public final static String ITEM_CATEGORY_NAME_EXISTS = "ItemCategory has been exists";
        public final static String ITEM_CATEGORY_NAME_NOT_FOUND = "ItemCategory could not be found";
        public final static String ITEM_CATEGORY_CREATE_SUCCESSFUL = "ItemCategory create successfully.";
        public final static String ITEM_CATEGORY_DELETE_SUCCESSFUL = "ItemCategory has deleted.";
        public final static String ITEM_CATEGORY_UPDATE_SUCCESSFUL = "ItemCategory has updated";
    }
    public static class ORDER {
        public static final String ORDER_NOT_FOUND = "Order could not be found";
        public static final String ORDER_CREATED = "Order created successfully";
        public static final String ORDER_DELETED = "Order has been deleted";
        public static final String ID_NOT_FOUND = "Order ID not found";
        public static final String ORDER_UPDATED = "Order updated";
        public static final String PAYMENT_CANNOT_UPDATE = "Payment Method can only be cash or bank";
        public static final String STATUS_CANNOT_UPDATE = "Order status can only be prepare,cooking,cancel or complete";

    }

    public static class CATEGORY{
        public  final static  String CATE_NAME_EXISTS = "Category has been exists";
        public static final String Category_NOT_FOUND = "Category could not be found";
        public static final String CATE_DELETED_SUCCESSFULLY = "Category has been deleted";
        public static final String CATE_ADD_SUCCESSFULLY =  "Category has been created";
        public static final String ASSOCIATE_WITH_FOOD = "Cannot delete foodCategory because it is associated with existing food items.";
    }
    public static class FOOD{
        public  final static  String FOOD_NAME_EXISTS = "Food has been exists";
        public static final String FOOD_NOT_FOUND = "Food could not be found";
        public static final String FOOD_DELETED_SUCCESSFULLY = "Food has been deleted";
        public static final String FOOD_ADD_SUCCESSFULLY =  "Foods have been created";
        public static final String FOOD_UPDATE_SUCCESSFULLY =  "Food has been updated";
        public static final String FOOD_CONSTRAINT =  "Cannot found foreign keys";
        public static final String FOOD_ADD_IMAGE_SUCCESSFULLY = "Image added successfully";
        public static final String FILE_IMAGE = "ImagePath existed";
        public static final String FOOD_HAVE_FOREIGN = "Id have already foreign with another entity ";
        public static final String FOOD_DELETE_IMAGE_SUCCESSFULLY = "Image deleted!";
        public static final String IMAGE_NOT_FOUND = "Image could not be found";
        public static final String IMAGE_DELETE = "Image deleted";
        public static final String FOOD_UPDATE_IMAGE_SUCCESSFULLY = "Image updated !";
        public static final String IMAGE_INVALID = "Invalid Image!";
        public static final String IMAGE_TOO_LARGE = "Image size too large";
        public static final String INVALID_FILTER = "Invalid Filter!";
        public static final String IMAGE_NOT_FOUND_IN_FOOD = "Image could be found in food, please upload first!";
        public static final String FOOD_CODE_EXISTS = "Food code has been exists";
    }
    public static class O_ITEM {
        public final static String O_ITEM_NAME_EXISTS = "OrderItem has been exists";
        public static final String O_ITEM_NOT_FOUND = "OrderItem could not be found";
        public static final String O_ITEM_DELETED_SUCCESSFULLY = "OrderItem has been deleted";
        public static final String O_ITEM_CONSTRAINT_USER = "Cannot found UserId";
        public static final String O_ITEM_CONSTRAINT_TABLE = "Cannot found TableId ";
        public static final String O_ITEM_CONSTRAINT_FOOD = "Cannot found foodID";
        public static final String O_ITEM_NOT_ASSOCIATED_WITH_TABLE = "foodId can not add in the same table";
    }


    public static class TABLE {
        public static final String TABLE_EXISTS = "Table already existed";
        public static final String TABLE_CREATED = "Table created successfully";
        public static final String TABLE_NOT_FOUND = "Table could not be found";
        public static final String ID_NOT_FOUND = "Table ID not found";
        public static final String CANNOT_CREATE = "Table can only be Available or Booked";
    }

    public static class SUPPLIER {
        public final static String SUPPLIER_NAME_EXISTS = "Supplier has been exists";
        public final static String SUPPLIER_NAME_NOT_FOUND = "Supplier could not be found";
        public final static String SUPPLIER_CREATE_SUCCESSFUL = "Supplier create successfully.";
        public final static String SUPPLIER_DELETE_SUCCESSFUL = "Supplier has deleted.";
        public final static String SUPPLIER_UPDATE_SUCCESSFUL = "Supplier has updated";
    }
}
