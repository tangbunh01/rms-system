package com.rms.system.awsS3;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "aws.s3.buckets")
@Data
public class S3Buckets {
    private String name;
    private String region;
    private String endPointUrl;
    private String accessKey;
    private String secretKey;
}
