package com.rms.system.controller;

import com.rms.system.base.BaseController;
import com.rms.system.base.BaseListingRQ;
import com.rms.system.base.StructureRS;
import com.rms.system.constant.MessageConstant;
import com.rms.system.model.request.food.FoodCategoryRQ;
import com.rms.system.service.FoodCategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/categories")
public class FoodCategoryController extends BaseController  {

    private final FoodCategoryService foodCategoryService;
    @GetMapping
    public ResponseEntity<StructureRS> store(BaseListingRQ request){
        return response(foodCategoryService.getCategory(request));
    }

    @PostMapping
    public ResponseEntity<StructureRS> add(@Validated @RequestBody FoodCategoryRQ foodCategoryRQ){
        foodCategoryService.addCategory(foodCategoryRQ);
        return response(MessageConstant.CATEGORY.CATE_ADD_SUCCESSFULLY);
    }

    @PutMapping("/{id}")
    public ResponseEntity<StructureRS> update(@Validated @RequestBody FoodCategoryRQ foodCategoryRQ, @PathVariable Long id){
        foodCategoryService.updateCategory(foodCategoryRQ,id);
        return response();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<StructureRS> delete(@PathVariable Long id){
        foodCategoryService.deleteCategory(id);
        return response(MessageConstant.CATEGORY.CATE_DELETED_SUCCESSFULLY);
    }
    @GetMapping("/{id}")
    public ResponseEntity<StructureRS> getById(@PathVariable Long id){
        return response(foodCategoryService.getId(id));
    }
}
