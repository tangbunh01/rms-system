package com.rms.system.controller;

import com.rms.system.base.BaseController;
import com.rms.system.base.BaseListingRQ;
import com.rms.system.base.StructureRS;
import com.rms.system.constant.MessageConstant;
import com.rms.system.model.request.food.FoodRQ;
import com.rms.system.service.FoodService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/foods")
@RequiredArgsConstructor
public class FoodController extends BaseController {
    private final FoodService foodService;

    @GetMapping
    public ResponseEntity<StructureRS> store(BaseListingRQ request){
        return response(foodService.getFoods(request));
    }
    @PostMapping
    public ResponseEntity<StructureRS> add(@Validated @RequestBody FoodRQ foodRQ){
        return response(foodService.addFood(foodRQ));
    }
    @PutMapping("/{id}")
    public ResponseEntity<StructureRS> update(@RequestBody FoodRQ foodRQ , @PathVariable Long id){
        foodService.updateFood(foodRQ,id);
        return response(MessageConstant.FOOD.FOOD_UPDATE_SUCCESSFULLY);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<StructureRS> delete(@PathVariable Long id){
        foodService.deleteFood(id);
        return response(MessageConstant.FOOD.FOOD_DELETED_SUCCESSFULLY);
    }
    @GetMapping("/{id}")
    public ResponseEntity<StructureRS> getById(@PathVariable Long id){
        return response(foodService.getByFoodId(id));
    }


    // Find the most popular foods
    @GetMapping("/list")
    public ResponseEntity<StructureRS> filter(@RequestParam(name = "filters",required = false) String filterFoods,@RequestParam(name = "nameType",required = false) String nameTypes  ){
        return response(foodService.filterFoods(filterFoods,nameTypes));
    }
}
