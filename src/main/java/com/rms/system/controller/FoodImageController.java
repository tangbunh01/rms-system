package com.rms.system.controller;


import com.rms.system.base.BaseController;
import com.rms.system.base.StructureRS;
import com.rms.system.service.FoodImageService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/food/images")
@RequiredArgsConstructor

public class FoodImageController extends BaseController {

    private final FoodImageService foodImageService;

    @PostMapping
    public ResponseEntity<StructureRS> uploadFoodImageById(@RequestParam("foodId")  Long foodId,
                                                           @RequestParam("files") MultipartFile files){
        return response(foodImageService.uploadFoodImageById(foodId, files));
    }

    @PutMapping("/{foodId}")
    public ResponseEntity<StructureRS> updateFoodImages(@RequestParam("file") MultipartFile file,
                                                        @PathVariable  Long foodId ){
        return  response(foodImageService.updateFoodImageByFoodId(file,foodId));
    }

    @DeleteMapping("/{foodId}")
    public ResponseEntity<StructureRS> deleteFoodImage(@PathVariable Long foodId){
        return response(foodImageService.deleteFoodImage(foodId));
    }

}
