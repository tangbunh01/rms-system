package com.rms.system.controller;

import com.rms.system.base.BaseController;
import com.rms.system.base.BaseListingRQ;
import com.rms.system.base.StructureRS;
import com.rms.system.model.request.user.*;
import com.rms.system.security.UserPrincipal;
import com.rms.system.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("api/user")
@RequiredArgsConstructor
public class UserController extends BaseController {

    private final UserService userService;

    @GetMapping
    public ResponseEntity<StructureRS> getUsers(
            BaseListingRQ request,
            @RequestParam(required = false, defaultValue = "0")
            Long roleId
    ) {
        return response(userService.getUsers(request, roleId));
    }

    @GetMapping("/customer")
    public ResponseEntity<StructureRS> getUserCustomer(BaseListingRQ request) {
        return response(userService.getUserCustomer(request));
    }

    //        @Secured("create-user")
//    @RolesAllowed("admin")
    @PostMapping
    public ResponseEntity<StructureRS> createUser(@Validated @RequestBody UserRQ userRQ) {
        return response(userService.createUser(userRQ));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<StructureRS> deleteUser(@PathVariable Long id) {
        return response(userService.deleteUser(id));
    }

    @PatchMapping("/{id}/status")
    public ResponseEntity<StructureRS> updateStatus(@PathVariable Long id) {
        return response(userService.updateStatus(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<StructureRS> updateUser(@PathVariable Long id, @Validated @RequestBody UpdateUserRQ request) {
        return response(userService.updateUser(id, request));
    }

    @GetMapping("/profile")
    public ResponseEntity<StructureRS> profile(JwtAuthenticationToken jwtAuthenticationToken) {
        return response(userService.profile(UserPrincipal.build(jwtAuthenticationToken)));
    }

    @PutMapping("/profile")
    public ResponseEntity<StructureRS> updateProfile(@Validated @RequestBody UpdateProfileRQ profileRQ,
                                                     JwtAuthenticationToken jwtAuthenticationToken) {
        return response(userService.updateProfile(profileRQ, UserPrincipal.build(jwtAuthenticationToken)));
    }

    @PatchMapping("/{id}/password")
    public ResponseEntity<StructureRS> changePasswordUser(@PathVariable Long id, @Validated @RequestBody PasswordChangeRQ request) {
        return response(userService.changePasswordUser(id, request));
    }

    @PatchMapping("/password/reset")
    public ResponseEntity<StructureRS> resetPassword(@Validated @RequestBody PasswordResetRQ request, JwtAuthenticationToken jwtAuthenticationToken) {
        return response(userService.resetPassword(request, UserPrincipal.build(jwtAuthenticationToken)));
    }

    @PostMapping(value = "/{id}/profile-avatar", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<StructureRS> uploadProfileImage(@PathVariable Long id, @RequestPart(value = "file") MultipartFile multipartFile) throws IOException {
        return response(userService.uploadProfileAvatar(id, multipartFile));
    }

    @DeleteMapping("/profile-avatar")
    public ResponseEntity<StructureRS> deleteProfileAvatar(JwtAuthenticationToken jwtAuthenticationToken) {
        return response(userService.deleteProfileAvatar(UserPrincipal.build(jwtAuthenticationToken)));
    }

    @PostMapping(value = "/profile-avatar/token", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<StructureRS> uploadProfileImageWithUserToken(JwtAuthenticationToken jwtAuthenticationToken,
                                                                       @RequestPart(value = "file") MultipartFile multipartFile) throws IOException {
        return response(userService.uploadProfileImageWithUserToken(UserPrincipal.build(jwtAuthenticationToken), multipartFile));
    }

    @GetMapping("/{id}")
    public ResponseEntity<StructureRS> getUserById(@PathVariable Long id) {
        return response(userService.getUserById(id));
    }

    @GetMapping("/order")
    public ResponseEntity<StructureRS> getUserOrder(BaseListingRQ requst, JwtAuthenticationToken jwt) {
        return response(userService.getUserOrder(requst, UserPrincipal.build(jwt)));
    }

    @PutMapping("/customer")
    public ResponseEntity<StructureRS> updateUserCustomer(@Validated @RequestBody UpdateUserCustomer request, JwtAuthenticationToken jwt) {
        return response(userService.updateUserCustomer(request, UserPrincipal.build(jwt)));
    }


}
