package com.rms.system.controller;


import com.rms.system.base.BaseController;
import com.rms.system.base.BaseListingRQ;
import com.rms.system.base.StructureRS;
import com.rms.system.model.request.role.RoleRQ;
import com.rms.system.model.request.role.UpdatePermissionRQ;
import com.rms.system.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/roles")
@RequiredArgsConstructor
public class RoleController extends BaseController {

    private final RoleService roleService;

    @GetMapping
    public ResponseEntity<StructureRS> getRoles(BaseListingRQ request) {
        return response(roleService.getRoles(request));
    }

    @PostMapping
    public ResponseEntity<StructureRS> createRole(@Validated @RequestBody RoleRQ roleRQ) {
        return response(roleService.createRole(roleRQ));
    }

    @PutMapping("/{id}")
    public ResponseEntity<StructureRS> updateRole(@Validated @RequestBody RoleRQ roleRQ, @PathVariable Long id) {
        StructureRS role = roleService.updateRole(roleRQ, id);
        return response(role.getData());
    }

    @PutMapping("/permission")
    public ResponseEntity<StructureRS> updatePermission(@RequestBody @Validated UpdatePermissionRQ updatePermissionRQ) {
        return response(roleService.updatePermission(updatePermissionRQ));
    }

    @GetMapping("/{id}")
    public ResponseEntity<StructureRS> getRoleById(@PathVariable Long id){
        return response(roleService.getRoleById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<StructureRS> deleteRoleById(@PathVariable Long id){
        return response(roleService.deleteRoleById(id));
    }
}
