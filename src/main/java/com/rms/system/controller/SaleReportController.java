package com.rms.system.controller;

import com.rms.system.base.*;
import com.rms.system.db.entity.enumentity.OrderPaymentMethodEnum;
import com.rms.system.model.request.role.RoleRQ;
import com.rms.system.service.SaleReportService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.Date;

@RestController
@RequiredArgsConstructor
@RequestMapping("/report")
public class SaleReportController extends BaseController {
    private final SaleReportService saleReportService;
    @GetMapping("/income")
    public ResponseEntity<StructureRS> getIncome(@RequestParam(value = "start", required = false) String startText,
                                                 @RequestParam(value = "end", required = false) String endText,
                                                 @RequestParam(value = "month",required = false) String monthText,
                                                 @RequestParam(value = "type",required = false) String list,
                                                 @RequestParam(value = "paymentStatus", required = false) OrderPaymentMethodEnum payment,
                                                 RangeTimeRQ timeReport, RoleRQ roleRQ, BaseListingRQ request){
        return response(saleReportService.getIncomeSold(startText,endText,monthText,list,payment,roleRQ,timeReport,request));
    }
    @GetMapping("/staff")
    public ResponseEntity<StructureRS> getStaff(@RequestParam(value = "start", required = false) String startText,
                                                @RequestParam(value = "end", required = false) String endText,
                                                @RequestParam(value = "month", required = false) String monthText,
                                                RangeTimeRQ timeReport, BaseListingRQ request){
        return response(saleReportService.getStaffSold(startText,endText,monthText,timeReport,request));
    }
}
