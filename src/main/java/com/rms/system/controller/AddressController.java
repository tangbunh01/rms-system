package com.rms.system.controller;

import com.rms.system.base.BaseController;
import com.rms.system.base.BaseListingRQ;
import com.rms.system.base.StructureRS;
import com.rms.system.model.request.address.AddressRQ;
import com.rms.system.security.UserPrincipal;
import com.rms.system.service.AddressService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/address")
@RequiredArgsConstructor
public class AddressController extends BaseController {

    private final AddressService addressService;

    @PostMapping("/{userId}")
    public ResponseEntity<StructureRS> createAddressByUser(@PathVariable Long userId, @Valid @RequestBody AddressRQ addressRQ){
        return response(addressService.createAddressByUser(userId, addressRQ));
    }

    @PostMapping
    public ResponseEntity<StructureRS> createAddressByUserLogin(@Valid @RequestBody AddressRQ addressRQ, JwtAuthenticationToken jwtAuthenticationToken){
        return response(addressService.createAddressByUserLogin(addressRQ, UserPrincipal.build(jwtAuthenticationToken)));
    }

    @GetMapping
    public ResponseEntity<StructureRS> getAddressByUserToken(BaseListingRQ request, JwtAuthenticationToken jwtAuthenticationToken){
        return response(addressService.getAddressByUserToken(request, UserPrincipal.build(jwtAuthenticationToken)));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<StructureRS> deleteAddressByUserToken(@PathVariable Long id, JwtAuthenticationToken jwtAuthenticationToken){
        return response(addressService.deleteAddressByUserToken(id, UserPrincipal.build(jwtAuthenticationToken)));
    }

    @GetMapping("/current")
    public ResponseEntity<StructureRS> getLastAddressToken(JwtAuthenticationToken jwt){
        return response(addressService.getAddressById(UserPrincipal.build(jwt)));
    }

}
