package com.rms.system.controller;

import com.rms.system.base.BaseController;
import com.rms.system.base.BaseListingRQ;
import com.rms.system.base.StructureRS;
import com.rms.system.db.entity.enumentity.TableStatusEnum;
import com.rms.system.model.request.table.TableRQ;
import com.rms.system.service.TableService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/tables")
@RequiredArgsConstructor
public class TableController extends BaseController {
    private final TableService tableService;
    @GetMapping()
    public ResponseEntity<StructureRS> getTable(BaseListingRQ request,@RequestParam(defaultValue = "All") TableStatusEnum status){
        return response(tableService.getTable(request,status));
    }
    @GetMapping("/{id}")
    public ResponseEntity<StructureRS> getTableById(@PathVariable Long id){
        return response(tableService.getTableById(id));
    }

    @PostMapping
    public ResponseEntity<StructureRS> createTable(@Validated @RequestBody TableRQ tableRQ){
        return response(tableService.createTable(tableRQ));
    }

    @PutMapping("/{id}")
    public ResponseEntity<StructureRS> updateTable(@Validated @PathVariable Long id,@RequestBody TableRQ tableRQ){
        return response(tableService.updateTable(id,tableRQ));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<StructureRS>  deleteTable(@PathVariable Long id){
        return response(tableService.deleteTable(id));

    }
}
