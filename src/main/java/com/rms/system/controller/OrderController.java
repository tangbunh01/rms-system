package com.rms.system.controller;

import com.rms.system.base.BaseController;
import com.rms.system.base.BaseListingRQ;
import com.rms.system.base.StructureRS;
import com.rms.system.db.entity.enumentity.OrderPaymentMethodEnum;
import com.rms.system.db.entity.enumentity.OrderStatusEnum;
import com.rms.system.model.request.order.OrderRQ;
import com.rms.system.model.request.order.OrderStatusRQ;
import com.rms.system.model.request.order.PaymentStatusRQ;
import com.rms.system.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/orders")
@RequiredArgsConstructor
public class OrderController extends BaseController {
    private final OrderService orderService;


    @GetMapping
    public ResponseEntity<StructureRS> getOrder(BaseListingRQ request,@RequestParam(defaultValue = "ALL") OrderStatusEnum status,@RequestParam(defaultValue = "ALL") OrderPaymentMethodEnum paymentMethod) {
        return response(orderService.getOrder(request,status,paymentMethod));
    }
    @GetMapping("/{id}")
    public ResponseEntity<StructureRS> getOrderById (@PathVariable Long id,BaseListingRQ request){
        return response(orderService.getOrderById(id,request));
    }
    @PostMapping
    public ResponseEntity<StructureRS> createOrder(@Validated @RequestBody OrderRQ orderRQ){
        return response(orderService.createOrder(orderRQ));
    }
    @PutMapping("/payment/{id}")
    public ResponseEntity<StructureRS> paymentMethod(@Validated @RequestBody PaymentStatusRQ paymentStatus, @PathVariable Long id){
        return response(orderService.paymentMethod(paymentStatus,id));
    }
    @PutMapping("/status/{id}")
    public ResponseEntity<StructureRS> orderStatus(@Validated @RequestBody OrderStatusRQ orderStatusRQ, @PathVariable Long id) {
        return response(orderService.orderStatus(orderStatusRQ, id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<StructureRS> updateOrder(@Validated @RequestBody OrderRQ orderRQ, @PathVariable Long id){
        return response(orderService.updateOrder(orderRQ,id));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<StructureRS> deleteOrder(@PathVariable Long id){
        return response(orderService.deleteOrder(id));

    }
}
