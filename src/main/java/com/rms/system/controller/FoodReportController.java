package com.rms.system.controller;

import com.rms.system.base.BaseController;
import com.rms.system.base.BaseListingRQ;
import com.rms.system.base.RangeTimeRQ;
import com.rms.system.base.StructureRS;
import com.rms.system.service.FoodReportService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequiredArgsConstructor
@RequestMapping("/report/food")
public class FoodReportController extends BaseController {
   private final FoodReportService foodReportService;

    @GetMapping
    public ResponseEntity<StructureRS> getFood(@RequestParam(value = "start",required = false) String startText,
                                               @RequestParam(value = "end",required = false) String endText,
                                               @RequestParam(value = "month" ,required = false) String monthText,
                                               @RequestParam(value = "food",required = false, defaultValue = "0") int top,
                                               RangeTimeRQ timeReport, BaseListingRQ request){
        return response(foodReportService.getFood(startText, endText,  monthText,top,timeReport, request));
    }
}
