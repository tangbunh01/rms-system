package com.rms.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RmsSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(RmsSystemApplication.class, args);
    }
    
}
