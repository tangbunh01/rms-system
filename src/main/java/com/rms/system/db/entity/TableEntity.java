package com.rms.system.db.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rms.system.base.BaseEntity;
import com.rms.system.db.entity.enumentity.TableStatusEnum;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SQLRestriction;
import org.hibernate.annotations.Where;

import java.util.List;

@Entity
@Table(name = "res_table", indexes = {
        @Index(name = "idx_table_name", columnList = "name")
})

@SQLDelete(sql = "UPDATE res_table SET deleted_at = now() WHERE id = ?")
@Getter
@Setter
public class TableEntity extends BaseEntity {

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "seat_capacity")
    private String seatCapacity;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private TableStatusEnum status;

    @JsonIgnore
    @OneToMany(mappedBy = "tableEntity")
    private List<OrderEntity>orderEntities;

}

