package com.rms.system.db.entity.enumentity;

public enum OrderStatusEnum {
    Prepare,
    Cooking,
    Complete,
    Cancel,
    ALL
}
