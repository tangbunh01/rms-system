package com.rms.system.db.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rms.system.base.BaseEntity;
import com.rms.system.db.entity.enumentity.UserGenderEnum;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SQLRestriction;
import org.hibernate.annotations.Where;

import java.time.Instant;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "user", indexes = {
        @Index(name = "idx_user_username", columnList = "username", unique = true),
        @Index(name = "idx_user_email", columnList = "email", unique = true)
})
@SQLDelete(sql = "UPDATE user SET deleted_at = now() WHERE id = ?")
@Getter
@Setter
public class UserEntity extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "gender", nullable = false)
    @Enumerated(EnumType.STRING)
    private UserGenderEnum gender;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "phone", nullable = false)
    private String phone;

    @Column(name = "salary")
    private Double salary;

    @Column(name = "hire_date", nullable = false)
    private Instant hireDate;

    @Column(name = "avatar")
    private String avatar;

    @Column(name = "bio", columnDefinition = "TEXT")
    private String bio;

    @Column(name = "status", columnDefinition = "tinyint default 1")
    private Boolean status = Boolean.TRUE;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "role_id", nullable = false, referencedColumnName = "id")
    private RoleEntity roleEntity;

    @JsonIgnore
    @OneToMany(mappedBy = "userEntity", fetch = FetchType.LAZY)
    private List<AddressEntity> addressEntities;

    @JsonIgnore
    @OneToMany(mappedBy = "userEntities", fetch = FetchType.LAZY)
    private List<StaffAttendanceEntity> staffAttendanceEntities;

    @JsonIgnore
    @OneToMany(mappedBy = "userEntity")
    private List<ItemEntity> itemEntities;

    @JsonIgnore
    @OneToMany(mappedBy = "userEntity")
    private List<OrderEntity> orderEntities;

}


