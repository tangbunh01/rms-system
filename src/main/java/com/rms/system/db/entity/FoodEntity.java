package com.rms.system.db.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rms.system.base.BaseEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SQLRestriction;
import org.hibernate.annotations.Where;

import java.util.List;

@Entity
@Table(name = "food", indexes = {
        @Index(name = "idx_food_name", columnList = "name"),
        @Index(name = "idx_food_code", columnList = "code", unique = true)
})
@SQLDelete(sql = "UPDATE food SET deleted_at = now() WHERE id = ?")
@Getter
@Setter
public class FoodEntity extends BaseEntity {

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "food_image")
    private String foodImage;

    @Column(name = "price", nullable = false)
    private Double price;

    @Column(name = "discount")
    private Double discount;

    @Column(name = "description")
    private String description;

    @Column(name = "status", columnDefinition = "tinyint default 1")
    private Boolean status = Boolean.TRUE;

    @Column(name = "count")
    private Integer count;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "food_category_id", nullable = false)
    private FoodCategoryEntity foodCategoryEntity;

    @JsonIgnore
    @OneToMany(mappedBy = "foodEntity")
    private List<OrderItemEntity> orderItemEntities;

    @JsonIgnore
    @OneToMany(mappedBy = "foodEntity", cascade = CascadeType.ALL)
    private List<FoodImageEntity> foodImageEntities;
}

