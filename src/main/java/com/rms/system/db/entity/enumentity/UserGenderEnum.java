package com.rms.system.db.entity.enumentity;

public enum UserGenderEnum {

        Male,
        Female,
        Other

}
