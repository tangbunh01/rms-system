package com.rms.system.db.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rms.system.base.BaseEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SQLRestriction;

@Entity
@Table(name = "address", indexes = {
        @Index(name = "idx_address_name", columnList = "name"),
        @Index(name = "idx_address_location", columnList = "location")
})
@SQLDelete(sql = "UPDATE address SET deleted_at = now() WHERE id = ?")
@Getter
@Setter
public class AddressEntity extends BaseEntity {

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "location", nullable = false)
    private String location;

    @Column(name = "google_map_url")
    private String googleMapUrl;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity userEntity;

}
