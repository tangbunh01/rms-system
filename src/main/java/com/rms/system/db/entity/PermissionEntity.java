package com.rms.system.db.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;

import java.util.Set;

@Entity
@Table(name = "permission", indexes = {
        @Index(name = "idx_permission_name", columnList = "name", unique = true)
})
@Getter
@Setter
public class PermissionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "module", nullable = false)
    private String module;

    @JsonIgnore
    @ManyToMany(mappedBy = "permissionEntities")
    private Set<RoleEntity> roleEntities;
}
