package com.rms.system.db.entity;

import com.rms.system.base.BaseEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SQLRestriction;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "food_image")
@SQLDelete(sql = "UPDATE food_image SET deleted_at = now() WHERE id = ?")
@Getter
@Setter
public class FoodImageEntity extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "food_id", referencedColumnName = "id")
    private FoodEntity foodEntity;

    @Column(name = "url", unique = true)
    private String url;
}
