package com.rms.system.db.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.sym.Name;
import com.rms.system.base.BaseEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SQLRestriction;
import org.hibernate.annotations.Where;

import java.io.PipedReader;
import java.util.List;

@Entity
@Table(name = "item_category", indexes = {
        @Index(name = "idx_item_category_name", columnList = "name", unique = true)
})
@SQLDelete(sql = "UPDATE item_category SET deleted_at = now() WHERE id = ?")
@Getter
@Setter
public class ItemCategoryEntity extends BaseEntity {

    @Column(name = "name", nullable = false)
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "itemCategoryEntity")
    private List<ItemEntity> itemEntities;


}
