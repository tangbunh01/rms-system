package com.rms.system.db.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.rms.system.base.BaseEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SQLRestriction;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;

import java.time.Instant;

@Entity
@Table(name = "staff_attendance")

@SQLDelete(sql = "UPDATE staff_attendance SET deleted_at = now() WHERE id = ?")
@Getter
@Setter
public class StaffAttendanceEntity extends BaseEntity {

    @CreatedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Column(name = "attendance_date", nullable = false)
    private Instant attendanceDate;

    @CreatedDate
    @JsonFormat(pattern = "HH:mm:ss")
    @Column(name = "clock_in_time", nullable = false)
    private Instant ClockInTime;

    @CreatedDate
    @JsonFormat(pattern = "HH:mm:ss")
    @Column(name = "clock_out_time", nullable = false)
    private Instant ClockOutTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity userEntities;

}
