package com.rms.system.db.entity.enumentity;

public enum TableStatusEnum {
    All,
    Available,
    Booked,
}
