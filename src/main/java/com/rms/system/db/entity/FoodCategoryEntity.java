package com.rms.system.db.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rms.system.base.BaseEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.SQLRestriction;
import org.hibernate.annotations.Where;

import java.util.List;

@Entity
@Table(name = "food_category", indexes = @Index(name = "idx_food_category_name", columnList = "name"))
@SQLDelete(sql = "UPDATE food_category SET deleted_at = now() WHERE id = ?")
@Getter
@Setter
public class FoodCategoryEntity extends BaseEntity {

    @Column(name = "name", nullable = false)
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "foodCategoryEntity")
    private List<FoodEntity> foodEntities;
}
