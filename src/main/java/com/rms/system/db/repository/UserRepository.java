package com.rms.system.db.repository;

import com.rms.system.db.entity.UserEntity;
import com.rms.system.model.projection.user.UserEntityInfo;
import com.rms.system.model.projection.user.UserEntityInfoById;
import com.rms.system.model.projection.user.UserEntityInfoProfile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
    @Query("""
            select u from UserEntity u
            join fetch u.roleEntity role
            left join fetch role.permissionEntities
            where u.username = :username and u.deletedAt is null
            """)
    UserEntity findByUsernameFetchRolePermission(@Param("username") String username);

    @Query("""
            select u from UserEntity u
            join fetch u.roleEntity
            where (:query = 'all' or u.name like concat(:query, '%')) and (:roleId = 0 or u.roleEntity.id = :roleId)
            and u.deletedAt is null
            and u.roleEntity.id != 2
            """)
    Page<UserEntityInfo> findUsers(String query, Long roleId, Pageable pageable);

    @Query("""
            select u from UserEntity u
            join fetch u.roleEntity
            where (:query = 'all' or u.name like concat(:query, '%'))
            and u.roleEntity.id = 2
            and u.deletedAt is null
            """)
    Page<UserEntityInfo> findCustomers(String query, Pageable pageable);

    Boolean existsAllByUsername(String username);

    Boolean existsAllByEmail(String email);

    @Query("select u from UserEntity u where u.id = :id and u.deletedAt is null")
    Optional<UserEntity> findByIdEqualsAndDeletedAtNull(@Param("id") Long id);

    @Query("""
            select u from UserEntity u
            left join AddressEntity a
            on u.id = a.userEntity.id
            left join OrderEntity o
            on u.id = o.userEntity.id
            where u.id = :id and u.status = true
            and a.deletedAt is null
            and o.deletedAt is null
            
            """)
    Optional<UserEntityInfoProfile> findByIdTokenEqualsAndStatusTrue(@Param("id") Long id);

    @Query("""
            select u from UserEntity u
            left join AddressEntity a
            on u.id = a.userEntity.id
            where u.id = :id and u.status = true
            and a.deletedAt is null
            """)
    Optional<UserEntityInfoById> findByIdEqualsAndStatusTrue(@Param("id") Long id);


}