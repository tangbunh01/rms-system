package com.rms.system.db.repository;

import com.rms.system.db.entity.OrderEntity;
import com.rms.system.db.entity.enumentity.OrderPaymentMethodEnum;
import com.rms.system.model.projection.report.IncomeReportInfo;
import com.rms.system.model.projection.report.SaleReportInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface SaleReportRepository extends JpaRepository<OrderEntity,Long> {

    @Query("SELECT SUM(o.totalPrice) AS totalPrice " +
            "FROM OrderEntity o " +
            "WHERE (EXTRACT(date FROM o.updateDate) = EXTRACT(date FROM :start)) " )
    List<IncomeReportInfo> findIncomeByDayOrNow(Date start);
    @Query("SELECT  SUM(o.totalPrice) AS totalPrice " +
            "FROM OrderEntity o " +
            "WHERE EXTRACT(date FROM o.updateDate) between EXTRACT(date FROM :start) and EXTRACT(date FROM :end) " )
    List<IncomeReportInfo> findIncomeBetweenDate(@Param("start") Date start, @Param("end") Date end);
    @Query("SELECT o.createdDate as incomeDate, SUM(o.totalPrice) AS totalPrice " +
            "FROM OrderEntity o " +
            "WHERE EXTRACT(date FROM o.updateDate) between EXTRACT(date FROM :start) and EXTRACT(date FROM :end) " +
            "group by EXTRACT(day FROM o.updateDate) " +
            "order by o.updateDate" )
    Page<IncomeReportInfo> findIncomeBetweenDateList(@Param("start") Date start, @Param("end") Date end,Pageable pageable);
    @Query("SELECT  o.paymentMethod as paymentMethod, SUM(o.totalPrice) AS totalPrice " +
            "FROM OrderEntity o " +
            "WHERE EXTRACT(date FROM o.updateDate) between EXTRACT(date FROM :start) and EXTRACT(date FROM :end) "  +
            "AND o.paymentMethod = :payment " +
            "GROUP BY o.paymentMethod " )
    List<IncomeReportInfo> findIncomeBetweenDateHasPayment(@Param("start") Date start, @Param("end") Date end, OrderPaymentMethodEnum payment);
    @Query("SELECT o.createdDate as incomeDate, SUM(o.totalPrice) AS totalPrice " +
            "FROM OrderEntity o " +
            "WHERE EXTRACT(month FROM o.updateDate) = EXTRACT(month FROM :month) and extract(year from o.updateDate) =  EXTRACT(year FROM :month) " +
            "group by EXTRACT(day FROM o.updateDate) " +
            "order by o.updateDate asc" )
    Page<IncomeReportInfo> findIncomeMonthList(@Param("month") Date month,Pageable pageable);
    @Query("SELECT o.paymentMethod as paymentMethod, SUM(o.totalPrice) as totalPrice " +
            "FROM OrderEntity o " +
            "WHERE EXTRACT(date FROM o.updateDate) = EXTRACT(date FROM :date)  " +
            "AND o.paymentMethod = :payment " +
            "GROUP BY o.paymentMethod ")
    List<IncomeReportInfo> findIncomeByDayHasPayment(Date date, OrderPaymentMethodEnum payment);
    @Query("SELECT o.paymentMethod as paymentMethod, SUM(o.totalPrice) as totalPrice " +
                "FROM OrderEntity o " +
                "WHERE (EXTRACT(month FROM o.updateDate) = EXTRACT(month FROM :month) " +
                "and extract(year from o.updateDate) = extract(year from :month)) "  +
                "AND o.paymentMethod = :payment " +
                "GROUP BY o.paymentMethod ")
    List<IncomeReportInfo> findIncomeByMonth(Date month, OrderPaymentMethodEnum payment);
    @Query("SELECT  SUM(o.totalPrice) as totalPrice " +
            "FROM OrderEntity o " +
            "WHERE EXTRACT(month FROM o.updateDate) = EXTRACT(month FROM :month) " +
            "and extract(year from o.updateDate) = extract(year from :month) ")
    List<IncomeReportInfo> findIncomeByMonthNonePayment(Date month);

    @Query("SELECT o,o.userEntity.id, o.userEntity as cashier, o.userEntity.username as username, SUM(o.totalPrice) AS totalPrice " +
            "FROM OrderEntity o " +
            "WHERE (EXTRACT(date FROM o.updateDate) = EXTRACT(date FROM :dateDay)) " +
            "and ((:query = 'all') OR o.userEntity.name LIKE CONCAT(:query, '%') or o.userEntity.username like concat(:query, '%') ) " +
            " group by o.userEntity.id" )
    Page<SaleReportInfo> findStaffSoldByDay(Date dateDay, String query, Pageable pageable);
    @Query("SELECT o, o.userEntity.id, o.userEntity as cashier, SUM(o.totalPrice) as totalPrice " +
            "FROM OrderEntity o " +
            "WHERE (EXTRACT(MONTH FROM o.updateDate) = EXTRACT(MONTH FROM :month) AND EXTRACT(YEAR FROM o.updateDate) = EXTRACT(YEAR FROM :month)) " +
            "and ((:query = 'all') OR o.userEntity.name LIKE CONCAT(:query, '%')) " +
            "GROUP BY o.userEntity.id")
    Page<SaleReportInfo> getStaffByMonth(Date month, String query, Pageable pageable);
   @Query("SELECT o, o.userEntity.id, o.userEntity AS cashier, SUM(o.totalPrice) AS totalPrice " +
            "FROM OrderEntity o " +
            "WHERE EXTRACT(date FROM o.updateDate) BETWEEN EXTRACT(date FROM :start) AND EXTRACT(date FROM :end) " +
            "and ((:query = 'all') OR o.userEntity.name LIKE CONCAT(:query, '%'))  " +
            "GROUP BY o.userEntity.id ")
    Page<SaleReportInfo> findStaffSoldBetweenDate(@Param("start") Date start, @Param("end") Date end, String query, Pageable pageable);

}

