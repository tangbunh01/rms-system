package com.rms.system.db.repository;

import com.rms.system.db.entity.FoodCategoryEntity;
import com.rms.system.model.projection.food.FoodCategoryEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface FoodCategoryEntityRepository extends JpaRepository<FoodCategoryEntity, Long> {
    @Query("select (count(f) > 0) from FoodCategoryEntity f where f.name like :name")
    boolean existsByNameLike(@Param("name") String name);

    @Query("select f from FoodCategoryEntity f left join fetch f.foodEntities fe where (:query = 'all' or f.name like concat('%',:query, '%') ) and f.deletedAt is null and fe.deletedAt is null ")
    Page<FoodCategoryEntityInfo> findByNameLike( String query, Pageable pageable);

    @Query("select f from FoodCategoryEntity f where f.id = :id")
    FoodCategoryEntityInfo findOneById(Long id);

    @Transactional
    @Modifying
    @Query("delete from FoodCategoryEntity f where f.id = :id")
    int deleteByIdEquals(@Param("id") Long id);

    @Query("select (count(f) > 0) from FoodCategoryEntity f where f.id = :id  ")
    boolean existByIds(@Param("id") Long id);

    @Query("select f from FoodCategoryEntity f where f.id = :id and f.deletedAt is null")
    Optional<FoodCategoryEntity> findByIdAndDeletedAtIsNull(@Param("id") Long id);


}