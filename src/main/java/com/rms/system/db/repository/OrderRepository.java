package com.rms.system.db.repository;

import com.rms.system.db.entity.OrderEntity;
import com.rms.system.db.entity.enumentity.OrderPaymentMethodEnum;
import com.rms.system.db.entity.enumentity.OrderStatusEnum;
import com.rms.system.model.projection.order.OrderEntityInfo;
import com.rms.system.model.projection.order.OrderEntityInfoById;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface OrderRepository extends JpaRepository<OrderEntity, Long> {
    @Query("""
            select o from OrderEntity o
            where(:query = 'all' or o.tableEntity.name like concat('%',:query,'%'))
            and (:status = 'ALL' or o.status = :status)
            and (:paymentMethod = 'ALL' or o.paymentMethod = :paymentMethod) and o.deletedAt is null""")
    Page<OrderEntityInfo> findByTableEntityNameAndStatusAndPaymentMethod(
            @Param("query") String query,
            @Param("status") OrderStatusEnum status,
            @Param("paymentMethod") OrderPaymentMethodEnum paymentMethod,
            Pageable pageable);

    @Query("select f from OrderEntity f where f.id = :id and f.deletedAt is null")
    Optional<OrderEntity> findByIdAndDeletedAtIsNull(@Param("id") Long id);

    @Query("select (count(f) > 0) from OrderEntity f where f.id = :id and f.deletedAt is null ")
    boolean existByIds(@Param("id") Long id);

    @Query("SELECT CASE WHEN COUNT(o) > 0 " +
            "AND o.deletedAt IS NULL THEN true " +
            "ELSE false END FROM OrderEntity o WHERE o.id = :id")
    Boolean checkOrderExistsAndNotDeleted(@Param("id") Long id);

    @Query("select o from OrderEntity o where (o.id = :id)  and o.deletedAt is null")
    Page<OrderEntityInfoById> findByOrderEntityId(@Param("id") Long id , Pageable pageable);

    @Query("select o from OrderEntity o where o.userEntity.id = :id and o.deletedAt is null")
    Page<OrderEntityInfo> findByIdEqualsAndDeletedAtNull(@Param("id") Long id, Pageable pageable);


}