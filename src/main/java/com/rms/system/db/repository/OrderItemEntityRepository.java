package com.rms.system.db.repository;

import com.rms.system.db.entity.OrderEntity;
import com.rms.system.db.entity.OrderItemEntity;
import com.rms.system.model.projection.order.OrderEntityInfoById;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
public interface OrderItemEntityRepository extends JpaRepository<OrderItemEntity, Long> {
    @Transactional
    @Modifying
    @Query("update OrderItemEntity o set o.deletedAt = now() where o.orderEntity.id = :id")
    void deleteByOrderEntity_Id(@Param("id") Long id);
    @Query("select o from OrderItemEntity o where o.orderEntity = :orderEntity and o.deletedAt is null")
    List<OrderItemEntity> findByOrderEntity(@Param("orderEntity") OrderEntity orderEntity);
}