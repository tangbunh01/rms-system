package com.rms.system.db.repository;

import com.rms.system.db.entity.FoodEntity;
import com.rms.system.db.entity.FoodImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface FoodImageEntityRepository extends JpaRepository<FoodImageEntity, Long> {
    @Query("select f from FoodImageEntity f where f.foodEntity.id = :id")
    List<FoodImageEntity> findByFoodImageEntityIdEquals(@Param("id") Long id);
}