package com.rms.system.db.repository;

import com.rms.system.db.entity.RoleEntity;
import com.rms.system.model.projection.role.RoleEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;


//@RepositoryRestResource(collectionResourceRel = "roles", path = "roles")
public interface RoleRepository extends JpaRepository<RoleEntity, Long> {
    @Query("""
            select r from RoleEntity r join fetch  r.createdBy join fetch r.updateBy
            where (:query = 'all' or r.name like concat(:query, '%'))
            and (:query = 'all' or r.code like concat(:query, '%'))
            and r.deletedAt is null
            """)
    Page<RoleEntityInfo> findByNameStartsWithAndCode(String query, Pageable pageable);

    boolean existsByIdAndDeletedAtIsNotNull(Long id);

    boolean existsByName(String name);

    boolean existsByCode(String code);

    @Query("select r from RoleEntity r left join fetch r.permissionEntities where r.id = ?1 and r.deletedAt is null")
    RoleEntity findByIdFetchPermission(Long id);

    @Query("select (count(r) > 0) from RoleEntity r where r.name = :name and r.id <> :id and r.deletedAt is null")
    boolean existsByNameEqualsAndIdNotEquals(@Param("name") String name, @Param("id") Long id);

    @Query("select (count(r) > 0) from RoleEntity r where r.code = :code and r.id <> :id and r.deletedAt is null")
    boolean existsByCodeEqualsAndIdNotEquals(@Param("code") String code, @Param("id") Long id);


    @Query("select r from RoleEntity r left join fetch r.permissionEntities where r.id = ?1 and r.deletedAt is null ")
    Optional<RoleEntity> findByIdFetchPermissionInfo(Long id);

    @Query("select r from RoleEntity r where r.id = :id and r.deletedAt is null  and r.deletedAt is null")
    Optional<RoleEntityInfo> findRoleEntityInfoByIdEquals(@Param("id") Long id);

}
