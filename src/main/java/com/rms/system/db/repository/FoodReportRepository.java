package com.rms.system.db.repository;

import com.rms.system.db.entity.OrderItemEntity;
import com.rms.system.model.projection.report.FoodReportInfo;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.Date;

public interface FoodReportRepository extends JpaRepository<OrderItemEntity, Long> {
    @Query( "SELECT o,o.foodEntity.id, o.foodEntity as food, o.foodEntity.code as code, o.unitPrice as unitPrice, SUM(o.quantity) as totalQuantitySold, SUM(o.totalPrice) as totalPrice " +
            "FROM OrderItemEntity o JOIN FETCH o.foodEntity " +
            "WHERE EXTRACT(date FROM o.updateDate) BETWEEN extract(date from :startDate)  AND extract(date from :endDate) " +
            "and ((:query = 'all') OR o.foodEntity.name LIKE CONCAT(:query, '%') OR o.foodEntity.code LIKE CONCAT(:query, '%'))" +
            "GROUP BY o.foodEntity.id ")
    Page<FoodReportInfo> findFoodBetweenDay(Date startDate, Date endDate,String query, Pageable pageable);
    @Query("SELECT oi, oi.foodEntity.id, oi.foodEntity as food, oi.foodEntity.code as code, oi.unitPrice as unitPrice, SUM(oi.quantity) as totalQuantitySold, SUM(oi.totalPrice) as totalPrice " +
            "FROM OrderItemEntity oi JOIN FETCH oi.foodEntity " +
            "WHERE EXTRACT(DATE FROM oi.updateDate) = EXTRACT(DATE FROM :dateNow) " +
            "and ((:query = 'all') or oi.foodEntity.name LIKE concat(:query, '%') OR oi.foodEntity.code LIKE CONCAT(:query, '%') ) " +
            "GROUP BY oi.foodEntity.id " )
    Page<FoodReportInfo> findFoodDaySold(Date dateNow,String query, Pageable pageable);
    @Query("SELECT oi, oi.foodEntity as food,oi.foodEntity.code as code, oi.unitPrice as unitPrice, SUM(oi.quantity) as totalQuantitySold, SUM(oi.totalPrice) as totalPrice " +
            "FROM OrderItemEntity oi JOIN FETCH oi.foodEntity " +
            "WHERE EXTRACT(MONTH FROM oi.updateDate) = EXTRACT(MONTH FROM :month) " +
            "AND EXTRACT(YEAR FROM oi.updateDate) = EXTRACT(YEAR FROM :month) " +
            "and ((:query = 'all') or oi.foodEntity.name LIKE concat(:query, '%') OR oi.foodEntity.code LIKE CONCAT(:query, '%')) " +
            "GROUP BY oi.foodEntity")
    Page<FoodReportInfo> findFoodMonthSold(Date month,String query, Pageable pageable);

}
