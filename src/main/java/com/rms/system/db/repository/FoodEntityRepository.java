package com.rms.system.db.repository;

import com.rms.system.db.entity.FoodEntity;
import com.rms.system.model.projection.food.FoodEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface FoodEntityRepository extends JpaRepository<FoodEntity, Long> {

        @Query("""
                select f from FoodEntity f
                left join fetch f.foodImageEntities fi
                where (:query = 'all' or f.name like concat('%',:query, '%'))
                and f.deletedAt is null and fi.deletedAt is null
                or (:query = 'all' or f.code like concat('%',:query, '%'))
                and f.deletedAt is null and fi.deletedAt is null""")
        Page<FoodEntityInfo> findByNameLike(@Param("query") String query,Pageable pageable);

        @Query("select (count(f) > 0) from FoodEntity f where f.code = :code ")
        boolean existsByCodeEquals(@Param("code") String code);

        @Query("select (count(f) > 0) from FoodEntity f where f.code = :code and f.id != :id")
        boolean existsByCodeEqualsAndIdNotEquals(@Param("code") String code, @Param("id") Long id);


        @Query("select (count(f) > 0) from FoodEntity f where f.id = :id and f.deletedAt is null ")
        boolean existByIdAndDeleteIsNull(@Param("id") Long id);

        @Query("select f from FoodEntity f where f.id = :id  ")
        FoodEntityInfo findOneById(@Param("id") Long id);

        @Query("select f from FoodEntity f where f.id in :ids and f.deletedAt is null ")
        List<FoodEntity> findByIdIn(@Param("ids") Collection<Long> ids);

        @Query("select (count(f) > 0) from FoodEntity f where f.id in :ids and f.deletedAt is null ")
        Boolean existsAllById(@Param("ids") List<Long> ids);

        @Query("select f from FoodEntity f where f.id = :id and f.deletedAt is null")
        Optional<FoodEntity> findByIdAndDeletedAtIsNull(@Param("id") Long id);
        @Query("select (count(f) > 0) from FoodCategoryEntity f where f.id = :id and f.deletedAt is null ")
        boolean existByIds(@Param("id") Long id);

        @Query("select f from FoodEntity f where f.foodCategoryEntity.id = :id and f.deletedAt is null ")
        List<FoodEntity> findByFoodCategoryEntityIdEquals(@Param("id") Long id);


        @Query("select f from FoodEntity f where f.createdDate >= :date order by f.createdDate DESC limit 8")
        List<FoodEntityInfo> findByCreatedDate(Instant date);

        @Query("select f from FoodEntity f order by f.foodCategoryEntity.name asc ")
        List<FoodEntityInfo> findByFoodCategoryEntity();
        @Query("select f from FoodEntity f where f.foodCategoryEntity.name like  concat('%',:nameType, '%') order by f.foodCategoryEntity.name asc ")
        List<FoodEntityInfo> findByFoodCategoryEntity_Name(@Param("nameType") String nameType);





        @Query("""
                    select fe from OrderItemEntity oi
                    inner join oi.foodEntity fe on oi.foodEntity.id = fe.id
                    group by  oi.foodEntity.id
                    order by COUNT(oi.foodEntity.id ) DESC limit 8""")
        List<FoodEntityInfo> findPopularFood();

        @Query("select f.id from FoodEntity f where f.id = :id")
        Optional<Long> findFoodByIdEquals(Long id);


}