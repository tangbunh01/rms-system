package com.rms.system.db.repository;

import com.rms.system.db.entity.TableEntity;
import com.rms.system.db.entity.enumentity.TableStatusEnum;
import com.rms.system.model.projection.table.TableEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TableRepository extends JpaRepository<TableEntity, Long> {
@Query("""
            select t from TableEntity t
            where ((:status = 'All' or t.status = :status) or (:query = 'all' or t.name like concat(:query, '%')))
            and (:query = 'all' or t.seatCapacity like concat(:query, '%'))
            and t.deletedAt is null
        """)
Page<TableEntityInfo> findByNameStartsWithOrSeatCapacityLikeAndStatusEquals(
        @Param("query") String name,
        @Param("status") TableStatusEnum status,
        Pageable pageable);

    @Query("select (count(t) > 0) from TableEntity t where t.name = ?1")
    boolean existsByName(String name);
    @Query("SELECT CASE WHEN COUNT(t) > 0 " +
            "AND t.deletedAt IS NULL THEN true " +
            "ELSE false END FROM TableEntity t WHERE t.id = :id")
    Boolean checkTableExistsAndNotDeleted(@Param("id") Long id);

    @Query("select (count(t) > 0) from TableEntity t where t.id = :id and t.deletedAt is null")
    boolean existsByIdAndDeletedAtIsNull(@Param("id") Long id);
    @Query("select t from TableEntity t where t.id = :id")
    TableEntityInfo findOneById(@Param("id") Long id);
}
