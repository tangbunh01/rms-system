package com.rms.system.db.repository;

import com.rms.system.db.entity.AddressEntity;
import com.rms.system.model.projection.address.AddressEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface AddressRepository extends JpaRepository<AddressEntity, Long> {
    @Query("select (count(a) > 0) from AddressEntity a where a.name = :name and a.userEntity.id = :id")
    boolean existsByNameEqualsAndIdEquals(@Param("name") String name, @Param("id") Long id);

    Optional<AddressEntity> findByIdAndDeletedAtIsNull(Long id);

    @Query("select a from AddressEntity a where a.id = :id and a.deletedAt is null")
    Optional<AddressEntityInfo> findByIdEqualsAndDeletedAtNull(@Param("id") Long id);


    @Query("select a from AddressEntity a where a.userEntity.id = :id")
    Page<AddressEntityInfo> findByUserEntity_IdEquals(@Param("id") Long id, Pageable pageable);

    List<AddressEntityInfo> findAllByUserEntityId(Long id);

    @Query("select a from AddressEntity a where a.userEntity.id = :id order by a.id desc limit 1")
    Optional<AddressEntityInfo> findAddressEntityInfoByUserId(@Param("id") Long id);

}
