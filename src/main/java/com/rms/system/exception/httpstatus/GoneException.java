package com.rms.system.exception.httpstatus;

import lombok.Getter;

public class GoneException extends RuntimeException {

    private final String message;

    @Getter
    private final Object data;

    public GoneException(String message, Object data) {
        this.message = message;
        this.data = data;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
