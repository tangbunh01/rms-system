package com.rms.system.exception.httpstatus;

import lombok.Getter;

public class ForbiddenException extends RuntimeException {

    private final String message;
    @Getter
    private Object data;

    public ForbiddenException(String message, Object data) {
        this.message = message;
        this.data = data;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
