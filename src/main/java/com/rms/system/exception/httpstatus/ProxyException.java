package com.rms.system.exception.httpstatus;

import lombok.Getter;

public class ProxyException extends RuntimeException {

    private final String message;
    @Getter
    private final Object data;

    public ProxyException(String message, Object data) {
        this.message = message;
        this.data = data;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
