package com.rms.system.base;

import com.rms.system.exception.httpstatus.BadRequestException;
import lombok.Setter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;


@Setter
public class RangeTimeRQ {
    public Date getRang(String dateText){
        boolean hasDay = dateText.split(":").length == 3;
        if(hasDay){
            try {
                return new SimpleDateFormat("yyyy:MM:dd").parse(dateText);
            }
            catch (ParseException e) {throw new BadRequestException("Invalid Date format. Please provide 'YYYY:MM:DD' format.");}
        }else{
            try {
                return new SimpleDateFormat("yyyy:MM").parse(dateText);
            } catch (ParseException e) {throw new BadRequestException("Invalid Date month format. Please provide 'YYYY:MM:dd' format.");}
        }
    }
    public String getDate(){
        LocalDate date1 = Instant.now().atZone(ZoneId.systemDefault()).toLocalDate();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy:MM:dd"); // Corrected the pattern
        return date1.format(formatter);
    }

}
