CREATE TABLE address
(
    id             BIGINT AUTO_INCREMENT NOT NULL,
    name           VARCHAR(255)          NOT NULL,
    location       VARCHAR(255)          NOT NULL,
    google_map_url VARCHAR(255)          NULL,
    user_id        BIGINT                NOT NULL,
    created_by     BIGINT                NULL,
    update_by      BIGINT                NULL,
    created_date   datetime              NOT NULL,
    update_date    datetime              NULL,
    deleted_at     datetime              NULL,
    CONSTRAINT pk_address PRIMARY KEY (id)
);

CREATE TABLE food
(
    id               BIGINT AUTO_INCREMENT NOT NULL,
    name             VARCHAR(255)          NOT NULL,
    code             VARCHAR(255)          NOT NULL,
    food_image       VARCHAR(255)          NULL,
    price            DOUBLE                NOT NULL,
    discount         DOUBLE                NULL,
    description      VARCHAR(255)          NULL,
    status           TINYINT DEFAULT 1     NULL,
    count            INT                   NULL,
    food_category_id BIGINT                NOT NULL,
    created_by       BIGINT                NULL,
    update_by        BIGINT                NULL,
    created_date     datetime              NOT NULL,
    update_date      datetime              NULL,
    deleted_at       datetime              NULL,
    CONSTRAINT pk_food PRIMARY KEY (id)
);

CREATE TABLE food_category
(
    id           BIGINT AUTO_INCREMENT NOT NULL,
    name         VARCHAR(255)          NOT NULL,
    created_by   BIGINT                NULL,
    update_by    BIGINT                NULL,
    created_date datetime              NOT NULL,
    update_date  datetime              NULL,
    deleted_at   datetime              NULL,
    CONSTRAINT pk_food_category PRIMARY KEY (id)
);

CREATE TABLE food_image
(
    id           BIGINT AUTO_INCREMENT NOT NULL,
    food_id      BIGINT                NULL,
    url          VARCHAR(255)          NULL,
    created_by   BIGINT                NULL,
    update_by    BIGINT                NULL,
    created_date datetime              NOT NULL,
    update_date  datetime              NULL,
    deleted_at   datetime              NULL,
    CONSTRAINT pk_food_image PRIMARY KEY (id)
);

CREATE TABLE item
(
    id               BIGINT AUTO_INCREMENT NOT NULL,
    name             VARCHAR(255)          NOT NULL,
    quantity         INT                   NOT NULL,
    unit_price       DOUBLE                NOT NULL,
    total_price      DOUBLE                NOT NULL,
    receive_date     datetime              NOT NULL,
    user_id          BIGINT                NOT NULL,
    supplier_id      BIGINT                NOT NULL,
    item_category_id BIGINT                NOT NULL,
    created_by       BIGINT                NULL,
    update_by        BIGINT                NULL,
    created_date     datetime              NOT NULL,
    update_date      datetime              NULL,
    deleted_at       datetime              NULL,
    CONSTRAINT pk_item PRIMARY KEY (id)
);

CREATE TABLE item_category
(
    id           BIGINT AUTO_INCREMENT NOT NULL,
    name         VARCHAR(255)          NOT NULL,
    created_by   BIGINT                NULL,
    update_by    BIGINT                NULL,
    created_date datetime              NOT NULL,
    update_date  datetime              NULL,
    deleted_at   datetime              NULL,
    CONSTRAINT pk_item_category PRIMARY KEY (id)
);

CREATE TABLE order_item
(
    id           BIGINT AUTO_INCREMENT NOT NULL,
    order_id     BIGINT                NOT NULL,
    food_id      BIGINT                NOT NULL,
    quantity     INT                   NOT NULL,
    unit_price   DOUBLE                NOT NULL,
    total_price  DOUBLE                NOT NULL,
    created_by   BIGINT                NULL,
    update_by    BIGINT                NULL,
    created_date datetime              NOT NULL,
    update_date  datetime              NULL,
    deleted_at   datetime              NULL,
    CONSTRAINT pk_order_item PRIMARY KEY (id)
);

CREATE TABLE orders
(
    id             BIGINT AUTO_INCREMENT NOT NULL,
    payment_method VARCHAR(255)          NOT NULL,
    total_price    DOUBLE                NOT NULL,
    status         VARCHAR(255)          NOT NULL,
    user_id        BIGINT                NOT NULL,
    table_id       BIGINT                NULL,
    created_by     BIGINT                NULL,
    update_by      BIGINT                NULL,
    created_date   datetime              NOT NULL,
    update_date    datetime              NULL,
    deleted_at     datetime              NULL,
    CONSTRAINT pk_orders PRIMARY KEY (id)
);

CREATE TABLE permission
(
    id     BIGINT AUTO_INCREMENT NOT NULL,
    name   VARCHAR(255)          NOT NULL,
    module VARCHAR(255)          NOT NULL,
    CONSTRAINT pk_permission PRIMARY KEY (id)
);

CREATE TABLE res_table
(
    id            BIGINT AUTO_INCREMENT NOT NULL,
    name          VARCHAR(255)          NOT NULL,
    seat_capacity VARCHAR(255)          NULL,
    status        VARCHAR(255)          NOT NULL,
    created_by    BIGINT                NULL,
    update_by     BIGINT                NULL,
    created_date  datetime              NOT NULL,
    update_date   datetime              NULL,
    deleted_at    datetime              NULL,
    CONSTRAINT pk_res_table PRIMARY KEY (id)
);

CREATE TABLE `role`
(
    id           BIGINT AUTO_INCREMENT NOT NULL,
    name         VARCHAR(255)          NOT NULL,
    code         VARCHAR(255)          NOT NULL,
    created_by   BIGINT                NULL,
    update_by    BIGINT                NULL,
    created_date datetime              NOT NULL,
    update_date  datetime              NULL,
    deleted_at   datetime              NULL,
    CONSTRAINT pk_role PRIMARY KEY (id)
);

CREATE TABLE roles_has_permissions
(
    permission_id BIGINT NOT NULL,
    role_id       BIGINT NOT NULL
);

CREATE TABLE staff_attendance
(
    id              BIGINT AUTO_INCREMENT NOT NULL,
    attendance_date datetime              NOT NULL,
    clock_in_time   datetime              NOT NULL,
    clock_out_time  datetime              NOT NULL,
    user_id         BIGINT                NOT NULL,
    created_by      BIGINT                NULL,
    update_by       BIGINT                NULL,
    created_date    datetime              NOT NULL,
    update_date     datetime              NULL,
    deleted_at      datetime              NULL,
    CONSTRAINT pk_staff_attendance PRIMARY KEY (id)
);

CREATE TABLE supplier
(
    id            BIGINT AUTO_INCREMENT NOT NULL,
    name          VARCHAR(255)          NOT NULL,
    email         VARCHAR(255)          NOT NULL,
    phone         VARCHAR(255)          NOT NULL,
    supplier_date datetime              NOT NULL,
    created_by    BIGINT                NULL,
    update_by     BIGINT                NULL,
    created_date  datetime              NOT NULL,
    update_date   datetime              NULL,
    deleted_at    datetime              NULL,
    CONSTRAINT pk_supplier PRIMARY KEY (id)
);

CREATE TABLE user
(
    id           BIGINT AUTO_INCREMENT NOT NULL,
    name         VARCHAR(255)          NULL,
    username     VARCHAR(255)          NOT NULL,
    gender       VARCHAR(255)          NOT NULL,
    email        VARCHAR(255)          NOT NULL,
    password     VARCHAR(255)          NOT NULL,
    phone        VARCHAR(255)          NOT NULL,
    salary       DOUBLE                NULL,
    hire_date    datetime              NOT NULL,
    avatar       VARCHAR(255)          NULL,
    bio          TEXT                  NULL,
    status       TINYINT DEFAULT 1     NULL,
    role_id      BIGINT                NOT NULL,
    created_by   BIGINT                NULL,
    update_by    BIGINT                NULL,
    created_date datetime              NOT NULL,
    update_date  datetime              NULL,
    deleted_at   datetime              NULL,
    CONSTRAINT pk_user PRIMARY KEY (id)
);

ALTER TABLE food_image
    ADD CONSTRAINT uc_food_image_url UNIQUE (url);

ALTER TABLE supplier
    ADD CONSTRAINT uc_supplier_phone UNIQUE (phone);

CREATE INDEX idx_address_location ON address (location);

CREATE INDEX idx_address_name ON address (name);

CREATE INDEX idx_food_category_name ON food_category (name);

CREATE UNIQUE INDEX idx_food_code ON food (code);

CREATE INDEX idx_food_name ON food (name);

CREATE UNIQUE INDEX idx_item_category_name ON item_category (name);

CREATE INDEX idx_item_name ON item (name);

CREATE UNIQUE INDEX idx_permission_name ON permission (name);

CREATE UNIQUE INDEX idx_role_code ON `role` (code);

CREATE UNIQUE INDEX idx_supplier_email ON supplier (email);

CREATE INDEX idx_supplier_name ON supplier (name);

CREATE INDEX idx_table_name ON res_table (name);

CREATE UNIQUE INDEX idx_user_email ON user (email);

CREATE UNIQUE INDEX idx_user_username ON user (username);

ALTER TABLE address
    ADD CONSTRAINT FK_ADDRESS_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES user (id),
    ADD CONSTRAINT FK_ADDRESS_ON_UPDATE_BY FOREIGN KEY (update_by) REFERENCES user (id),
    ADD CONSTRAINT FK_ADDRESS_ON_USER FOREIGN KEY (user_id) REFERENCES user (id);

ALTER TABLE food_category
    ADD CONSTRAINT FK_FOOD_CATEGORY_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES user (id),
    ADD CONSTRAINT FK_FOOD_CATEGORY_ON_UPDATE_BY FOREIGN KEY (update_by) REFERENCES user (id);

ALTER TABLE food_image
    ADD CONSTRAINT FK_FOOD_IMAGE_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES user (id),
    ADD CONSTRAINT FK_FOOD_IMAGE_ON_FOOD FOREIGN KEY (food_id) REFERENCES food (id),
    ADD CONSTRAINT FK_FOOD_IMAGE_ON_UPDATE_BY FOREIGN KEY (update_by) REFERENCES user (id);

ALTER TABLE food
    ADD CONSTRAINT FK_FOOD_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES user (id),
    ADD CONSTRAINT FK_FOOD_ON_FOOD_CATEGORY FOREIGN KEY (food_category_id) REFERENCES food_category (id),
    ADD CONSTRAINT FK_FOOD_ON_UPDATE_BY FOREIGN KEY (update_by) REFERENCES user (id);

ALTER TABLE item_category
    ADD CONSTRAINT FK_ITEM_CATEGORY_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES user (id),
    ADD CONSTRAINT FK_ITEM_CATEGORY_ON_UPDATE_BY FOREIGN KEY (update_by) REFERENCES user (id);

ALTER TABLE item
    ADD CONSTRAINT FK_ITEM_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES user (id),
    ADD CONSTRAINT FK_ITEM_ON_ITEM_CATEGORY FOREIGN KEY (item_category_id) REFERENCES item_category (id),
    ADD CONSTRAINT FK_ITEM_ON_SUPPLIER FOREIGN KEY (supplier_id) REFERENCES supplier (id),
    ADD CONSTRAINT FK_ITEM_ON_UPDATE_BY FOREIGN KEY (update_by) REFERENCES user (id),
    ADD CONSTRAINT FK_ITEM_ON_USER FOREIGN KEY (user_id) REFERENCES user (id);

ALTER TABLE orders
    ADD CONSTRAINT FK_ORDERS_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES user (id),
    ADD CONSTRAINT FK_ORDERS_ON_TABLE FOREIGN KEY (table_id) REFERENCES res_table (id),
    ADD CONSTRAINT FK_ORDERS_ON_UPDATE_BY FOREIGN KEY (update_by) REFERENCES user (id),
    ADD CONSTRAINT FK_ORDERS_ON_USER FOREIGN KEY (user_id) REFERENCES user (id);

ALTER TABLE order_item
    ADD CONSTRAINT FK_ORDER_ITEM_ON_FOOD FOREIGN KEY (food_id) REFERENCES food (id),
    ADD CONSTRAINT FK_ORDER_ITEM_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES user (id),
    ADD CONSTRAINT FK_ORDER_ITEM_ON_ORDER FOREIGN KEY (order_id) REFERENCES orders (id),
    ADD CONSTRAINT FK_ORDER_ITEM_ON_UPDATE_BY FOREIGN KEY (update_by) REFERENCES user (id);

ALTER TABLE res_table
    ADD CONSTRAINT FK_RES_TABLE_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES user (id),
    ADD CONSTRAINT FK_RES_TABLE_ON_UPDATE_BY FOREIGN KEY (update_by) REFERENCES user (id);

ALTER TABLE `role`
    ADD CONSTRAINT FK_ROLE_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES user (id),
    ADD CONSTRAINT FK_ROLE_ON_UPDATE_BY FOREIGN KEY (update_by) REFERENCES user (id);

ALTER TABLE staff_attendance
    ADD CONSTRAINT FK_STAFF_ATTENDANCE_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES user (id),
    ADD CONSTRAINT FK_STAFF_ATTENDANCE_ON_UPDATE_BY FOREIGN KEY (update_by) REFERENCES user (id),
    ADD CONSTRAINT FK_STAFF_ATTENDANCE_ON_USER FOREIGN KEY (user_id) REFERENCES user (id);

ALTER TABLE supplier
    ADD CONSTRAINT FK_SUPPLIER_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES user (id),
    ADD CONSTRAINT FK_SUPPLIER_ON_UPDATE_BY FOREIGN KEY (update_by) REFERENCES user (id);

ALTER TABLE user
    ADD CONSTRAINT FK_USER_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES user (id),
    ADD CONSTRAINT FK_USER_ON_ROLE FOREIGN KEY (role_id) REFERENCES `role` (id),
    ADD CONSTRAINT FK_USER_ON_UPDATE_BY FOREIGN KEY (update_by) REFERENCES user (id);

ALTER TABLE roles_has_permissions
    ADD CONSTRAINT fk_role_has_per_on_permission_entity FOREIGN KEY (permission_id) REFERENCES permission (id),
    ADD CONSTRAINT fk_role_has_per_on_role_entity FOREIGN KEY (role_id) REFERENCES `role` (id);

