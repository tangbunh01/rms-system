insert into role (id, name, code, created_by, update_by, created_date, update_date)
values (1, 'Super-Admin', 'super-admin', null, null, current_timestamp, current_timestamp),
       (2, 'User', 'user', null, null, current_timestamp, current_timestamp);

insert into permission (id, name, module)
values (1, 'list-role', 'role'),
       (2, 'create-role', 'role'),
       (3, 'edit-role', 'role'),
       (4, 'delete-role', 'role'),
       (5, 'list-user', 'user'),
       (6, 'create-user', 'user'),
       (7, 'edit-user', 'user'),
       (8, 'delete-user', 'user'),
       (9, 'list-orderItem', 'orderItem'),
       (10, 'create-orderItem', 'orderItem'),
       (11, 'edit-orderItem', 'orderItem'),
       (12, 'delete-orderItem', 'orderItem'),
       (13, 'list-food', 'food'),
       (14, 'create-food', 'food'),
       (15, 'edit-food', 'food'),
       (16, 'delete-food', 'food'),
       (17, 'list-foodCategory', 'foodCategory'),
       (18, 'create-foodCategory', 'foodCategory'),
       (19, 'edit-foodCategory', 'foodCategory'),
       (20, 'delete-foodCategory', 'foodCategory'),
       (21, 'list-order', 'order'),
       (22, 'create-order', 'order'),
       (23, 'edit-order', 'order'),
       (24, 'delete-order', 'order'),
       (25, 'list-table', 'table'),
       (26, 'create-table', 'table'),
       (27, 'edit-table', 'table'),
       (28, 'delete-table', 'table');


insert into roles_has_permissions (permission_id, role_id)
values (1, 1),
       (2, 1),
       (3, 1),
       (4, 1),
       (5, 1),
       (6, 1),
       (7, 1),
       (8, 1);
insert into user (id, name, username, gender, email, password, phone, salary, hire_date, avatar,bio, status, role_id,
                  created_by, update_by, created_date, update_date)
values (1, 'Admin', 'admin', 'Male', 'admin@gmail.com',
        '{bcrypt}$2a$10$wZkctmC/rOezW2B3Zr0Wj.R3zNqDt9wPjIwKU/q7OM1fJsvVN3gFW', '0987654321', 5000, current_timestamp,
        'profile photo', 'Hello everyone', 1, 1, 1, 1, current_timestamp, current_timestamp),
       (2, 'User', 'user', 'Male', 'user@gmail.com',
        '{bcrypt}$2a$10$wZkctmC/rOezW2B3Zr0Wj.R3zNqDt9wPjIwKU/q7OM1fJsvVN3gFW', '0987654321', 5000, current_timestamp,
        'profile photo', 'Hello everyone', 1, 1, 1, 1, current_timestamp, current_timestamp);

update role set created_by =1, update_by=1 where id = 1;
update role set created_by =1, update_by=1 where id = 2;
